class RedirectionRoutes:
    def __init__(self, personal_gitlab, cloud_arpanode, mattermost_arpanode, monitoring, tensorboard, trello,
                 extranet, hyperplanning, ydays, ymatch, outlook, gitlab, linkedin, instagram, markdownify, artemis):
        self.personal_gitlab = personal_gitlab
        self.cloud_arpanode = cloud_arpanode
        self.mattermost_arpanode = mattermost_arpanode
        self.monitoring = monitoring
        self.tensorboard = tensorboard
        self.trello = trello
        self.extranet = extranet
        self.hyperplanning = hyperplanning
        self.ydays = ydays
        self.ymatch = ymatch
        self.outlook = outlook
        self.gitlab = gitlab
        self.linkedin = linkedin
        self.instagram = instagram
        self.markdownify = markdownify
        self.artemis = artemis
    def to_dict(self):
        return {
            'personal_gitlab': self.personal_gitlab,
            'cloud_arpanode': self.cloud_arpanode,
            'mattermost_arpanode': self.mattermost_arpanode,
            'monitoring': self.monitoring,
            'tensorboard': self.tensorboard,
            'trello': self.trello,
            'extranet': self.extranet,
            'hyperplanning': self.hyperplanning,
            'ydays': self.ydays,
            'ymatch': self.ymatch,
            'outlook': self.outlook,
            'gitlab': self.gitlab,
            'linkedin': self.linkedin,
            'instagram': self.instagram,
            'markdownify': self.markdownify,
            'artemis': self.artemis
        }

    def __str__(self):
        return f"RedirectionRoutes(personal_gitlab={self.personal_gitlab}, cloud_arpanode={self.cloud_arpanode}," \
               f" mattermost_arpanode={self.mattermost_arpanode}, monitoring={self.monitoring}," \
               f" tensorboard={self.tensorboard}, trello={self.trello}, extranet={self.extranet}, " \
               f"hyperplanning={self.hyperplanning}, ydays={self.ydays}, ymatch={self.ymatch}, outlook={self.outlook}," \
               f" gitlab={self.gitlab}, linkedin={self.linkedin}, instagram={self.instagram}, markdownify={self.markdownify}," \
                f" artemis={self.artemis})"