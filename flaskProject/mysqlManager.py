import mysql.connector
from mysql.connector import errorcode
import bcrypt
from userManager import User
from projectManager import Project, Part
from aboutmeManager import AboutMe, ContactInfo
from routeManager import RedirectionRoutes


class MySQLManager:
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.cnx = None
        self.cursor = None

    def connect(self):
        try:
            self.cnx = mysql.connector.connect(host=self.host,
                                               user=self.user,
                                               password=self.password,
                                               database=self.database,
                                               autocommit=True)
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def close(self):
        self.cursor.close()
        self.cnx.close()

    def execute(self, query, args=None):
        if args is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, args)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def get_last_row_id(self):
        return self.cursor.lastrowid

    def get_row_count(self):
        return self.cursor.rowcount

    # --------------------- User ---------------------
    def check_user_exists(self, username, email):
        self.connect()
        query = "SELECT * FROM users WHERE name = %s OR email = %s"
        self.execute(query, (username, email))
        user = self.fetchone()
        if user is not None:
            return True
        else:
            return False

    def register(self, username, password, email, image):
        self.connect()
        password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        query = "INSERT INTO users (name, password, email,profile_image,token) VALUES (%s, %s, %s, %s,%s)"
        self.execute(query, (username, password_hash, email, image, None))
        self.close()

    def check_userName_exists(self, username):
        self.connect()
        query = "SELECT * FROM users WHERE name = %s"
        self.execute(query, (username,))
        user = self.fetchone()
        if user is not None:
            return True
        else:
            return False

    def check_email_exists(self, email):
        self.connect()
        query = "SELECT * FROM users WHERE email = %s"
        self.execute(query, (email,))
        user = self.fetchone()
        if user is not None:
            return True
        else:
            return False
    def add_token(self, token, email):
        self.connect()
        query = "UPDATE users SET token = %s WHERE email = %s"
        self.execute(query, (token, email))
        self.close()
    def get_token(self, token):
        self.connect()
        query = "SELECT * FROM users WHERE token = %s"
        self.execute(query, (token,))
        user = self.fetchone()
        if user is not None:
            return self.tuple_to_user(user)
        else:
            return None
    def remove_token(self, userId):
        self.connect()
        query = "UPDATE users SET token = %s WHERE id = %s"
        self.execute(query, (None, userId))
        self.close()

    def get_mail_by_token(self, token):
        self.connect()
        query = "SELECT email FROM users WHERE token = %s"
        self.execute(query, (token,))
        user = self.fetchone()
        if user is not None:
            return self.tuple_to_user(user)
        else:
            return None

    def check_if_user_exist_update(self, username, email, id):
        self.connect()
        query = "SELECT * FROM users WHERE (name = %s OR email = %s) AND id != %s"
        self.execute(query, (username, email, id))
        user = self.fetchone()
        self.close()

        if user is not None:
            return True
        else:
            return False


    def update_user(self, username, email, image, id):
        if not self.check_if_user_exist_update(username, email, id):
            try:
                self.connect()
                query = "UPDATE users SET name = %s, email = %s ,profile_image = %s WHERE id = %s"
                self.execute(query, (username, email, image, id))
                self.close()
            except Exception as e:
                print(e)
                raise e
        else:
            print("User already exists")
            return False
        return True



    def update_user_role(self, role, id):
        self.connect()
        query = "UPDATE users SET role = %s WHERE id = %s"
        self.execute(query, (role, id))
        self.close()

    def update_user_password(self, password, id):
        query = "UPDATE users SET password = %s WHERE id = %s"
        password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        try:
            self.connect()
            self.execute(query, (password_hash, id))
            self.close()
        except Exception as e:
            print(e)
            raise e



    def login(self, username, password):
        self.connect()
        query = "SELECT * FROM users WHERE name = %s OR email = %s"
        self.execute(query, (username, username))
        user = self.fetchone()
        if user is not None:
            password_hash = user[2]
            if bcrypt.checkpw(password.encode('utf-8'), password_hash.encode('utf-8')):
                return self.tuple_to_user(user)
        return None

    def get_user_by_id(self, id):
        self.connect()
        query = "SELECT * FROM users WHERE id = %s"
        self.execute(query, (id,))
        user = self.fetchone()
        if user is not None:
            return self.tuple_to_user(user)
        else:
            return None

    def get_all_users(self):
        self.connect()
        self.execute("SELECT * FROM users")
        users = self.fetchall()
        userlist = []
        for user in users:
            userlist.append(self.tuple_to_user(user))
        return userlist

    def remove_user_by_id(self, id):
        self.connect()
        query = "DELETE FROM users WHERE id = %s"
        self.execute(query, (id,))
        self.close()

    def tuple_to_user(self, user):
        return User(user[0], user[1], user[2], user[3], user[4], user[5], user[6])

    def user_to_tuple(self, user):
        return (user.id, user.name, user.password, user.email, user.role, user.image, user.token)

    def print_users(self):
        self.connect()
        self.execute("SELECT * FROM users")
        users = self.cursor.fetchall()
        for user in users:
            print(user)
        self.close()

    def createTableUser(self):
        self.connect()
        self.execute(
            "CREATE TABLE IF NOT EXISTS users (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), password VARCHAR(255), email VARCHAR(255), role VARCHAR(255), profile_image VARCHAR(255))")
        self.close()

    # -----------------------------PROJECTS---------------------------------------------
    def createTableProject(self):
        self.connect()
        self.execute(
            "CREATE TABLE IF NOT EXISTS projects (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), image VARCHAR(255), type VARCHAR(255), gitlab_link VARCHAR(255), active VARCHAR(255), created_at DATETIME, team_role VARCHAR(255), team_size VARCHAR(255), image_list VARCHAR(255), movies_list VARCHAR(255), conclusion VARCHAR(255))")
        self.close()

    def project_to_tuple(self, project):
        return (
            project.id, project.name, project.description, project.image, project.type, project.gitlab_link,
            project.active,
            project.created_at, project.team_role, project.team_size, project.image_list, project.movies_list,
            project.conclusion)

    def tuple_to_project(self, project):
        project = Project(id=project[0], name=project[1], description=project[2], image=project[3], type=project[4],
                       gitlab_link=project[5], active=project[6], created_at=project[7], team_role=project[8],
                       team_size=project[9], image_list=project[10].split(","), movies_list=project[11].split(","), conclusion=project[12])
        for image in project.image_list:
            if image == "":
                project.image_list.remove(image)
        for movie in project.movies_list:
            if movie == "":
                project.movies_list.remove(movie)
        return project
    def get_all_projects(self):
        self.connect()
        self.execute("SELECT * FROM projects")
        projects = self.fetchall()
        projectlist = []
        for project in projects:
            projectlist.append(self.tuple_to_project(project))
        return projectlist

    def print_all_projects(self):
        self.connect()
        self.execute("SELECT * FROM projects")
        projects = self.cursor.fetchall()
        for project in projects:
            print(project)
        self.close()

    def get_project_by_id(self, id):
        self.connect()
        query = "SELECT * FROM projects WHERE id = %s"
        self.execute(query, (id,))
        project = self.fetchone()
        if project is not None:
            return self.tuple_to_project(project)
        else:
            return None

    def get_project_main_image(self, id):
        self.connect()
        query = "SELECT image FROM projects WHERE id = %s"
        self.execute(query, (id,))
        image = self.fetchone()
        if image is not None:
            return image[0]
        else:
            return None
    def get_project_images(self, id):
        self.connect()
        query = "SELECT image_list FROM projects WHERE id = %s"
        self.execute(query, (id,))
        images = self.fetchone()
        if images is not None:
            return images[0].split(",")
        else:
            return []
    def get_project_movies(self, id):
        self.connect()
        query = "SELECT movies_list FROM projects WHERE id = %s"
        self.execute(query, (id,))
        movies = self.fetchone()
        if movies is not None:
            return movies[0].split(",")
        else:
            return []
    def get_project_name(self, id):
        self.connect()
        query = "SELECT name FROM projects WHERE id = %s"
        self.execute(query, (id,))
        name = self.fetchone()
        if name is not None:
            return name[0]
        else:
            return None

    def get_project_by_name(self, name):
        self.connect()
        query = "SELECT * FROM projects WHERE name = %s"
        self.execute(query, (name,))
        project = self.fetchone()
        if project is not None:
            return self.tuple_to_project(project)
        else:
            return None

    def get_project_by_type(self, type):
        self.connect()
        query = "SELECT * FROM projects WHERE type = %s"
        self.execute(query, (type,))
        projects = self.fetchall()
        projectlist = []
        for project in projects:
            projectlist.append(self.tuple_to_project(project))
        return projectlist

    def get_project_by_active(self, active):
        self.connect()
        query = "SELECT * FROM projects WHERE active = %s"
        self.execute(query, (active,))
        project = self.fetchone()
        if project is not None:
            return self.tuple_to_project(project)
        else:
            return None

    def get_project_by_created_at(self, created_at):
        self.connect()
        query = "SELECT * FROM projects WHERE created_at = %s"
        self.execute(query, (created_at,))
        project = self.fetchone()
        if project is not None:
            return self.tuple_to_project(project)
        else:
            return None

    def get_project_by_team_role(self, team_role):
        self.connect()
        query = "SELECT * FROM projects WHERE team_role = %s order by created_at desc"
        self.execute(query, (team_role,))
        project = self.fetchone()
        if project is not None:
            return self.tuple_to_project(project)
        else:
            return None

    def get_latest_projects(self, limit):
        self.connect()
        query = "SELECT * FROM projects order by created_at desc limit %s"
        self.execute(query, (limit,))
        projects = self.fetchall()
        projectlist = []
        for project in projects:
            projectlist.append(self.tuple_to_project(project))
        return projectlist

    def get_two_latest_projects_by_type(self, type):
        self.connect()
        query = "SELECT * FROM projects WHERE type = %s order by created_at desc limit 2"
        self.execute(query, (type,))
        projects = self.fetchall()
        projectlist = []
        for project in projects:
            projectlist.append(self.tuple_to_project(project))
        return projectlist

    def remove_project_by_id(self, id):
        self.connect()
        query = "DELETE FROM projects WHERE id = %s"
        self.execute(query, (id,))
        self.close()

    def remove_all_projects(self):
        self.connect()
        query = "DELETE FROM projects"
        self.execute(query)
        self.close()

    def update_project(self, project):
        self.connect()
        query = "UPDATE projects SET name = %s, description = %s, image = %s, type = %s, gitlab_link = %s, active = %s, created_at = %s, team_role = %s, team_size = %s, image_list = %s, movies_list = %s, conclusion = %s WHERE id = %s"
        self.execute(query, (
            project.name, project.description, project.image, project.type, project.gitlab_link, project.active,
            project.created_at, project.team_role, project.team_size, project.image_list, project.movies_list,
            project.conclusion, project.id))
        self.close()

    def insert_project(self, project):
        self.connect()
        query = "INSERT INTO projects (name, description, image, type, gitlab_link, active, created_at, team_role, team_size, image_list, movies_list, conclusion) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)"
        values = (project.name, project.description, project.image, project.type, project.gitlab_link, project.active,
                  project.created_at, project.team_role, project.team_size, project.image_list, project.movies_list,
                  project.conclusion)
        self.execute(query, values)
        # return new project id
        self.execute("SELECT LAST_INSERT_ID()")
        project_id = self.fetchone()[0]
        self.close()
        return project_id


    def update_project_SmartMirror(self):
        base_project = self.get_project_by_id(21)
        project = Project(id=base_project.id, name=base_project.name, description=base_project.team_role,
                          image=base_project.movies_list, type=base_project.image, gitlab_link=base_project.description,
                          active=base_project.type, created_at=base_project.created_at,
                          team_role=base_project.gitlab_link,
                          team_size=base_project.active, image_list=base_project.conclusion,
                          movies_list=base_project.parts,
                          conclusion=base_project.image_list)
        self.insert_project(project)

    # -----------------------------PARTS---------------------------------------------
    def updateParts(self, project_id, new_project_id):
        self.connect()
        query = "UPDATE parts SET project_id = %s WHERE project_id = %s"
        self.execute(query, (new_project_id, project_id))
        self.close()

    def createTablePart(self):
        self.connect()
        self.execute(
            "CREATE TABLE IF NOT EXISTS parts (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), description MEDIUMTEXT, image VARCHAR(255), project_id INT, code MEDIUMTEXT, conclusion MEDIUMTEXT)")
        self.close()

    def part_to_tuple(self, part):
        # if part image_list start or end with , remove it
        if part.image[0] == ",":
            part.image = part.image[1:]
        if part.image[-1] == ",":
            part.image = part.image[:-1]

        # if movie_list start or end with , remove it
        if part.movies_list[0] == ",":
            part.movies_list = part.movies_list[1:]
        if part.movies_list[-1] == ",":
            part.movies_list = part.movies_list[:-1]

        return (part.id, part.title, part.description, part.image, part.project_id,part.codeLanguage, part.code, part.conclusion, part.partIndex)

    def tuple_to_part(self, part):
        part = Part(id=part[0], title=part[1], description=part[2],codeLanguage=part[3], image=part[4].split(','), project_id=part[5], code=part[6],
                    conclusion=part[7], partIndex=part[8])
        for image in part.image:
            if image == "":
                part.image.remove(image)
        return part
    def print_all_parts(self):
        self.connect()
        self.execute("SELECT * FROM parts")
        parts = self.fetchall()
        for part in parts:
            print(part)
            self.close()

    def get_all_parts(self):
        self.connect()
        self.execute("SELECT * FROM parts")
        parts = self.fetchall()
        partlist = []
        for part in parts:
            partlist.append(self.tuple_to_part(part))
        return partlist

    def get_part_by_id(self, id):
        self.connect()
        query = "SELECT * FROM parts WHERE id = %s"
        self.execute(query, (id,))
        part = self.fetchone()
        if part is not None:
            return self.tuple_to_part(part)
        else:
            return None

    def get_project_id(self,part_id):
        self.connect()
        query = "SELECT project_id FROM parts WHERE id = %s"
        self.execute(query, (part_id,))
        part = self.fetchone()
        if part is not None:
            return part[0]
        else:
            return None
    def get_part_images(self, id):
        self.connect()
        query = "SELECT image FROM parts WHERE id = %s"
        self.execute(query, (id,))
        part = self.fetchone()
        if part is not None:
            return part[0].split(',')
        else:
            return []

    def get_parts_by_project_id(self, project_id):
            self.connect()
            query = "SELECT * FROM parts WHERE project_id = %s ORDER BY partIndex ASC"
            self.execute(query, (project_id,))
            parts = []
            for part_tuple in self.fetchall():
                part = self.tuple_to_part(part_tuple)
                parts.append(part)
            return parts

    def remove_part_by_id(self, id):
        self.connect()
        query = "DELETE FROM parts WHERE id = %s"
        self.execute(query, (id,))
        self.close()

    def remove_part_by_project_id(self, project_id):
        self.connect()
        query = "DELETE FROM parts WHERE project_id = %s"
        self.execute(query, (project_id,))
        self.close()

    def cleen_parts(self):
        self.connect()
        query = "DELETE FROM parts WHERE project_id IS NULL"
        self.execute(query)
        self.close()

    def remove_all_parts(self):
        self.connect()
        query = "DELETE FROM parts"
        self.execute(query)
        self.close()

    def update_part(self, part):
        self.connect()
        query = "UPDATE parts SET title = %s, description = %s, codeLanguage = %s, image = %s, project_id = %s, code = %s, conclusion = %s , partIndex= %s WHERE id = %s"
        self.execute(query, (part.title, part.description, part.codeLanguage, part.image, part.project_id, part.code, part.conclusion,part.partIndex, part.id))
        self.close()


    def remove_part_by_project_id(self, project_id):
        self.connect()
        query = "DELETE FROM parts WHERE project_id = %s"
        self.execute(query, (project_id,))
        self.close()

    def insert_part(self, part):
        self.connect()
        query = "INSERT INTO parts (title, description,codeLanguage, image, project_id, code, conclusion,partIndex) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        self.execute(query, (part.title, part.description, part.codeLanguage, part.image, part.project_id, part.code, part.conclusion, part.partIndex))
        self.close()



    # -----------------------------image checks---------------------------------------------
    def check_image_used(self, image_path, project_id,part_id):
        # Check if the image is used in the projects table (main image or image list)
        self.connect()

        # Query to check if the image path exists in another project or image_list
        project_query = f"SELECT COUNT(*) FROM projects WHERE (image = '{image_path}' OR image_list LIKE '%{image_path}%') AND id != {project_id}"
        self.execute(project_query)
        project_count = self.cursor.fetchone()[0]

        # Query to check if the image path exists in another part
        if part_id is None:
           pass
        else:
            part_query = f"SELECT COUNT(*) FROM parts WHERE (image LIKE '%{image_path}%') AND id != {part_id}"
            self.execute(part_query)
            part_count = self.cursor.fetchone()[0]

            # check if image is used in users table (profile_image)
            user_query = f"SELECT COUNT(*) FROM users WHERE profile_image = '{image_path}'"
            self.execute(user_query)
            user_count = self.cursor.fetchone()[0]

            # check if image is present twice in this part (image)
            # get the part image list
            images_path_query = f"SELECT image FROM parts WHERE id = {part_id}"
            self.cursor.execute(images_path_query)
            images_path = self.cursor.fetchone()[0].split(',')
            # count the number of times the image is present in the list
            image_count = images_path.count(image_path)

            self.close()
            # if the image is used in another project or part or user or present twice in this part, return True
            if project_count > 0 or part_count > 0 or user_count > 0 or image_count > 1:
                return True

        return False

    def check_movie_used(self,movie, project_id):
        # Check if the movie is used in the projects table (movie list)
        query = "SELECT * FROM projects WHERE movies_list LIKE %s AND id != %s"
        self.execute(query, ('%' + movie + '%', project_id))
        project = self.fetchone()
        if project is not None:
            return True

        return False


     # -----------------------------about me---------------------------------------------

    def tuple_to_about_me(self, about_me_tuple):
        about_me = AboutMe()
        about_me.id = about_me_tuple[0]
        about_me.firstname = about_me_tuple[1]
        about_me.lastname = about_me_tuple[2]
        about_me.date_of_birth = about_me_tuple[3]
        about_me.introduction = about_me_tuple[4]
        about_me.background = about_me_tuple[5]
        about_me.experience = about_me_tuple[6]
        about_me.hard_skills = about_me_tuple[7].split(',')
        about_me.soft_skills = about_me_tuple[8].split(',')
        about_me.achievements = about_me_tuple[9].split(',')
        about_me.testimonials = about_me_tuple[10].split(';')
        about_me.target_job = about_me_tuple[12]
        about_me.resume = about_me_tuple[13]
        about_me.languages = about_me_tuple[14].split(',')
        about_me.pseudo = about_me_tuple[15]
        about_me.hobbies = about_me_tuple[16].split(',')
        about_me.testimonials_author = about_me_tuple[17].split(';')

        about_me.hard_skills_infra = about_me_tuple[18].split(',')
        about_me.hard_skills_infra_highlights = about_me_tuple[19].split(',')
        about_me.hard_skills_network = about_me_tuple[20].split(',')
        about_me.hard_skills_network_highlights = about_me_tuple[21].split(',')
        about_me.hard_skills_web = about_me_tuple[22].split(',')
        about_me.hard_skills_web_highlights = about_me_tuple[23].split(',')
        about_me.hard_skills_mobile = about_me_tuple[24].split(',')
        about_me.hard_skills_mobile_highlights = about_me_tuple[25].split(',')
        about_me.hard_skills_ai = about_me_tuple[26].split(',')
        about_me.hard_skills_ai_highlights = about_me_tuple[27].split(',')
        about_me.hard_skills_dev = about_me_tuple[28].split(',')
        about_me.hard_skills_dev_highlights = about_me_tuple[29].split(',')
        about_me.hard_skills_data = about_me_tuple[30].split(',')
        about_me.hard_skills_data_highlights = about_me_tuple[31].split(',')
        about_me.hard_skills_cloud = about_me_tuple[32].split(',')
        about_me.hard_skills_cloud_highlights = about_me_tuple[33].split(',')
        about_me.hard_skills_security = about_me_tuple[34].split(',')
        about_me.hard_skills_security_highlights = about_me_tuple[35].split(',')
        about_me.hard_skills_electronic = about_me_tuple[36].split(',')
        about_me.hard_skills_electronic_highlights = about_me_tuple[37].split(',')
        about_me.hard_skills_mechatronics = about_me_tuple[38].split(',')
        about_me.hard_skills_mechatronics_highlights = about_me_tuple[39].split(',')
        about_me.hard_skills_embedded_dev = about_me_tuple[40].split(',')
        about_me.hard_skills_embedded_dev_highlights = about_me_tuple[41].split(',')
        about_me.hard_skills_other = about_me_tuple[42].split(',')
        about_me.hard_skills_other_highlights = about_me_tuple[43].split(',')
        about_me.soft_skills_work = about_me_tuple[44].split(',')
        about_me.hard_skills_os = about_me_tuple[45].split(',')
        about_me.hard_skills_os_highlights = about_me_tuple[46].split(',')

        return about_me

    def about_me_to_tuple(self, about_me):
        hard_skills_str = ','.join(about_me.hard_skills)
        soft_skills_str = ','.join(about_me.soft_skills)
        languages_str = ','.join(about_me.languages)
        achievements_str = ','.join(about_me.achievements)
        testimonials_str = ';'.join(about_me.testimonials)
        testimonials_author_str = ';'.join(about_me.testimonials_author)
        about_me.hobbies = ','.join(about_me.hobbies)
        hard_skills_str_infra = ','.join(about_me.hard_skills_infra)
        hard_skills_str_infra_highlights = ','.join(about_me.hard_skills_infra_highlights)
        hard_skills_str_network = ','.join(about_me.hard_skills_network)
        hard_skills_str_network_highlights = ','.join(about_me.hard_skills_network_highlights)
        hard_skills_str_web = ','.join(about_me.hard_skills_web)
        hard_skills_str_web_highlights = ','.join(about_me.hard_skills_web_highlights)
        hard_skills_str_mobile = ','.join(about_me.hard_skills_mobile)
        hard_skills_str_mobile_highlights = ','.join(about_me.hard_skills_mobile_highlights)
        hard_skills_str_ai = ','.join(about_me.hard_skills_ai)
        hard_skills_str_ai_highlights = ','.join(about_me.hard_skills_ai_highlights)
        hard_skills_str_dev = ','.join(about_me.hard_skills_dev)
        hard_skills_str_dev_highlights = ','.join(about_me.hard_skills_dev_highlights)
        hard_skills_str_data = ','.join(about_me.hard_skills_data)
        hard_skills_str_data_highlights = ','.join(about_me.hard_skills_data_highlights)
        hard_skills_str_cloud = ','.join(about_me.hard_skills_cloud)
        hard_skills_str_cloud_highlights = ','.join(about_me.hard_skills_cloud_highlights)
        hard_skills_str_security = ','.join(about_me.hard_skills_security)
        hard_skills_str_security_highlights = ','.join(about_me.hard_skills_security_highlights)
        hard_skills_str_electronic = ','.join(about_me.hard_skills_electronic)
        hard_skills_str_electronic_highlights = ','.join(about_me.hard_skills_electronic_highlights)
        hard_skills_str_mechatronics = ','.join(about_me.hard_skills_mechatronics)
        hard_skills_str_mechatronics_highlights = ','.join(about_me.hard_skills_mechatronics_highlights)
        hard_skills_str_embedded_dev = ','.join(about_me.hard_skills_embedded_dev)
        hard_skills_str_embedded_dev_highlights = ','.join(about_me.hard_skills_embedded_dev_highlights)
        hard_skills_str_other = ','.join(about_me.hard_skills_other)
        hard_skills_str_other_highlights = ','.join(about_me.hard_skills_other_highlights)
        soft_skills_str_work = ','.join(about_me.soft_skills_work)
        hard_skills_str_os = ','.join(about_me.hard_skills_os)
        hard_skills_str_os_highlights = ','.join(about_me.hard_skills_os_highlights)

        about_me_tuple = (
            about_me.firstname,
            about_me.lastname,
            about_me.date_of_birth,
            about_me.introduction,
            about_me.background,
            about_me.experience,
            hard_skills_str,
            soft_skills_str,
            languages_str,
            achievements_str,
            testimonials_str,
            1,
            about_me.target_job,
            about_me.resume,
            about_me.pseudo,
            about_me.hobbies,
            testimonials_author_str,
            hard_skills_str_infra,
            hard_skills_str_infra_highlights,
            hard_skills_str_network,
            hard_skills_str_network_highlights,
            hard_skills_str_web,
            hard_skills_str_web_highlights,
            hard_skills_str_mobile,
            hard_skills_str_mobile_highlights,
            hard_skills_str_ai,
            hard_skills_str_ai_highlights,
            hard_skills_str_dev,
            hard_skills_str_dev_highlights,
            hard_skills_str_data,
            hard_skills_str_data_highlights,
            hard_skills_str_cloud,
            hard_skills_str_cloud_highlights,
            hard_skills_str_security,
            hard_skills_str_security_highlights,
            hard_skills_str_electronic,
            hard_skills_str_electronic_highlights,
            hard_skills_str_mechatronics,
            hard_skills_str_mechatronics_highlights,
            hard_skills_str_embedded_dev,
            hard_skills_str_embedded_dev_highlights,
            hard_skills_str_other,
            hard_skills_str_other_highlights,
            soft_skills_str_work,
            hard_skills_str_os,
            hard_skills_str_os_highlights
        )

        return about_me_tuple


    def get_about_me(self):
        self.connect()
        query = "SELECT * FROM about_me"
        self.execute(query)
        about_me_tuple = self.fetchone()
        self.close()
        if about_me_tuple is None:
            return None
        return self.tuple_to_about_me(about_me_tuple)


    def insert_about_me(self, about_me):
        self.connect()
        query = "INSERT INTO about_me (firstname, lastname, date_of_birth, introduction, background, experience, " \
                "hard_skills, soft_skills, languages, achievements, testimonials, contact_info_id, target_job, " \
                "resume, pseudo, hobbies, testimonials_author, hard_skills_infra, hard_skills_infra_highlights, " \
                "hard_skills_network, hard_skills_network_highlights, hard_skills_web, hard_skills_web_highlights, " \
                "hard_skills_mobile, hard_skills_mobile_highlights, hard_skills_ai, hard_skills_ai_highlights, " \
                "hard_skills_dev, hard_skills_dev_highlights, hard_skills_data, hard_skills_data_highlights, " \
                "hard_skills_cloud, hard_skills_cloud_highlights, hard_skills_security, hard_skills_security_highlights, " \
                "hard_skills_electronic, hard_skills_electronic_highlights, hard_skills_mechatronics, " \
                "hard_skills_mechatronics_highlights, hard_skills_embedded_dev, hard_skills_embedded_dev_highlights, " \
                "hard_skills_other, hard_skills_other_highlights, soft_skills_work, hard_skills_os, " \
                "hard_skills_os_highlights) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " \
                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        about_me_tuple = self.about_me_to_tuple(about_me)
        self.execute(query, about_me_tuple)
        self.close()

    def remove_about_me(self):
        self.connect()
        query = "DELETE FROM about_me"
        self.execute(query)
        self.close()

    def update_about_me(self, about_me):
        self.connect()
        query = "UPDATE about_me SET \
             firstname = %s, \
             lastname = %s, \
             date_of_birth = %s, \
             introduction = %s, \
             background = %s, \
             experience = %s, \
             hard_skills = %s, \
             soft_skills = %s, \
             languages = %s, \
             achievements = %s, \
             testimonials = %s, \
             contact_info_id = %s, \
             target_job = %s, \
             resume = %s, \
             pseudo = %s, \
             hobbies = %s, \
             testimonials_author = %s, \
             hard_skills_infra = %s, \
             hard_skills_infra_highlights = %s, \
             hard_skills_network = %s, \
             hard_skills_network_highlights = %s, \
             hard_skills_web = %s, \
             hard_skills_web_highlights = %s, \
             hard_skills_mobile = %s, \
             hard_skills_mobile_highlights = %s, \
             hard_skills_ai = %s, \
             hard_skills_ai_highlights = %s, \
             hard_skills_dev = %s, \
             hard_skills_dev_highlights = %s, \
             hard_skills_data = %s, \
             hard_skills_data_highlights = %s, \
             hard_skills_cloud = %s, \
             hard_skills_cloud_highlights = %s, \
             hard_skills_security = %s, \
             hard_skills_security_highlights = %s, \
             hard_skills_electronic = %s, \
             hard_skills_electronic_highlights = %s, \
             hard_skills_mechatronics = %s, \
             hard_skills_mechatronics_highlights = %s, \
             hard_skills_embedded_dev = %s, \
             hard_skills_embedded_dev_highlights = %s, \
             hard_skills_other = %s, \
             hard_skills_other_highlights = %s, \
             soft_skills_work = %s, \
             hard_skills_os = %s, \
             hard_skills_os_highlights = %s \
             WHERE id = 1"

        about_me_tuple = self.about_me_to_tuple(about_me)
        self.execute(query, about_me_tuple)
        self.close()


    # -----------------------------contact---------------------------------------------

    def contact_to_tuple(self, contact):
        contact_tuple = (
            contact.personal_email,
            contact.school_email,
            contact.phone,
            contact.address,
            contact.lexit_email

        )
        return contact_tuple
    def tuple_to_contact(self, contact_tuple):
        contact = ContactInfo(
            personal_email=contact_tuple[1],
            school_email=contact_tuple[2],
            phone=contact_tuple[3],
            address=contact_tuple[4],
            lexit_email=contact_tuple[5])
        return contact

    def get_contact_info(self):
        self.connect()
        query = "SELECT * FROM contact_info"
        self.execute(query)
        contact_info_tuple = self.fetchone()
        self.close()
        if contact_info_tuple is None:
            return None
        return self.tuple_to_contact(contact_info_tuple)


    def insert_contact_info(self, contact_info):
        self.connect()
        query = "INSERT INTO contact_info (personal_email, school_email, phone, address,lexit_email) VALUES (%s, %s, %s, %s,%s)"
        contact_info_tuple = self.contact_to_tuple(contact_info)
        self.execute(query, contact_info_tuple)

        # Retrieve the ID of the inserted row
        contact_info_id = self.cursor.lastrowid

        self.close()

        return contact_info_id




    def remove_contact_info(self):
        self.connect()
        query = "DELETE FROM contact_info"
        self.execute(query)
        self.close()

    def update_contact_info(self, contact_info):
        self.connect()
        query = "UPDATE contact_info SET personal_email = %s, school_email = %s, phone = %s, address = %s, lexit_email = %s WHERE id = 1"
        contact_info_tuple = self.contact_to_tuple(contact_info)
        contact_info_tuple = contact_info_tuple
        self.execute(query, contact_info_tuple)
        self.close()


    # -----------------------------routes---------------------------------------------

    def route_to_tuple(self,route_obj):
        route_tuple = (
            route_obj.personal_gitlab,
            route_obj.cloud_arpanode,
            route_obj.mattermost_arpanode,
            route_obj.monitoring,
            route_obj.tensorboard,
            route_obj.trello,
            route_obj.extranet,
            route_obj.hyperplanning,
            route_obj.ydays,
            route_obj.ymatch,
            route_obj.outlook,
            route_obj.gitlab,
            route_obj.linkedin,
            route_obj.instagram,
            route_obj.markdownify,
            route_obj.artemis,
        )
        return route_tuple

    def tuple_to_route(self,route_tuple):
        route = RedirectionRoutes(
            personal_gitlab=route_tuple[1],
            cloud_arpanode=route_tuple[2],
            mattermost_arpanode=route_tuple[3],
            monitoring=route_tuple[4],
            tensorboard=route_tuple[5],
            trello=route_tuple[6],
            extranet=route_tuple[7],
            hyperplanning=route_tuple[8],
            ydays=route_tuple[9],
            ymatch=route_tuple[10],
            outlook=route_tuple[11],
            gitlab=route_tuple[12],
            linkedin=route_tuple[13],
            instagram=route_tuple[14],
            markdownify=route_tuple[15],
            artemis=route_tuple[16],
        )
        return route

    def get_route(self):
        self.connect()
        query = "SELECT * FROM redirection_routes"
        self.execute(query)
        route_tuple = self.fetchone()
        self.close()
        if route_tuple is None:
            return None
        return self.tuple_to_route(route_tuple)

    def insert_route(self, route):
        self.connect()
        query = "INSERT INTO redirection_routes (personal_gitlab, cloud_arpanode, mattermost_arpanode, monitoring, tensorboard, trello, extranet, hyperplanning, ydays, ymatch, outlook, gitlab, linkedin, instagram,markdownify, artemis) VALUES (%s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        route_tuple = self.route_to_tuple(route)
        self.execute(query, route_tuple)

        # Retrieve the ID of the inserted row
        route_id = self.cursor.lastrowid

        self.close()

        return route_id

    def remove_route(self):
        self.connect()
        query = "DELETE FROM redirection_routes"
        self.execute(query)
        self.close()

    def update_route(self, route):
        self.connect()
        query = "UPDATE redirection_routes SET personal_gitlab = %s, cloud_arpanode = %s, mattermost_arpanode = %s, monitoring = %s, tensorboard = %s, trello = %s, extranet = %s, hyperplanning = %s, ydays = %s, ymatch = %s, outlook = %s, gitlab = %s, linkedin = %s, instagram = %s, markdownify = %s, artemis = %s WHERE id = 1"
        route_tuple = self.route_to_tuple(route)
        route_tuple = route_tuple
        self.execute(query, route_tuple)
        self.close()


