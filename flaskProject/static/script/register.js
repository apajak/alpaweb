function checkConfirmPassword() {
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm-password").value;

    // Check if passwords match
    const passwordMatchText = passwordMatch(password, confirm_password);
    const passwordMatchIndicator = document.getElementById("password-match-indicator");
    passwordMatchIndicator.innerText = passwordMatchText;
    passwordMatchIndicator.className = `password-match-indicator ${passwordMatchText.toLowerCase()}`;
}

function passwordMatch(password, confirm_password) {
    if (password != confirm_password) {
        return "Passwords do not match!"
    } else {
        document.getElementById("confirm-password").setCustomValidity("");
        return ""
    }
}

function checkPassword() {
    var password = document.getElementById("password").value;

    // Check password strength
    const passwordStrength = calculatePasswordStrength(password);
    const passwordStrengthText = getPasswordStrengthText(passwordStrength);
    const passwordStrengthIndicator = document.getElementById("password-strength-indicator");
    passwordStrengthIndicator.innerText = passwordStrengthText;
    passwordStrengthIndicator.className = `password-strength-indicator ${passwordStrengthText.toLowerCase()}`;
}

function calculatePasswordStrength(password) {
    // Vérifier la longueur du mot de passe
    if (password.length < 8) {
        return 0; // Faible
    }

    // Vérifier la présence de chiffres et de lettres minuscules et majuscules
    if (!/[0-9]/.test(password) || !/[a-z]/.test(password) || !/[A-Z]/.test(password)) {
        return 1; // Moyen
    }

    // Vérifier la présence de caractères spéciaux
    if (!/[\W]/.test(password)) {
        return 2; // Fort
    }

    // Si le mot de passe satisfait tous les critères ci-dessus, il est très fort
    return 3;
}

function getPasswordStrengthText(passwordStrength) {
    switch (passwordStrength) {
        case 0:
            return "Very-weak";
        case 1:
            return "weak";
        case 2:
            return "Moderate";
        case 3:
            return "Strong";
        default:
            return "";
    }
}

function checkUserName() {
    var username = document.getElementById("username").value;
    const usernameIndicator = document.getElementById("username-indicator");
    if (!/^[a-zA-Z0-9]+$/.test(username)) {
        usernameIndicator.innerText = "Username can only contain alphanumeric characters.";
        usernameIndicator.className = "username-indicator invalid";
    } else if (username.length > 20) {
        usernameIndicator.innerText = "Username cannot exceed 20 characters.";
        usernameIndicator.className = "username-indicator invalid";
    } else {
        usernameIndicator.innerText = "";
        usernameIndicator.className = "username-indicator";
    }
}

function checkMail() {
    var email = document.getElementById("email").value;
    const emailIndicator = document.getElementById("email-indicator");
    if (!/\S+@\S+\.\S+/.test(email)) {
        emailIndicator.innerText = "Please enter a valid email address.";
        emailIndicator.className = "email-indicator invalid";
    } else {
        emailIndicator.innerText = "";
        emailIndicator.className = "email-indicator";
    }
}

function validateForm() {
    var username = document.getElementById("username").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm-password").value;

    // Validate username
    if (!/^[a-zA-Z0-9]+$/.test(username)) {
        alert("Username can only contain alphanumeric characters.");
        return false;
    }

    // Validate email
    if (!/\S+@\S+\.\S+/.test(email)) {
        alert("Please enter a valid email address.");
        return false;
    }

    // Check if password contains username or email
    if (password.includes(username) || password.includes(email)) {
        alert("Password should not contain your username or email address.");
        return false;
    }

    // Check if all fields are filled
    if (username == "" || email == "" || password == "" || confirm_password == "") {
        alert("All fields are required!");
        return false;
    }

    // Check if passwords match
    if (password != confirm_password) {
        alert("Passwords do not match!");
        return false;
    }

    // Check password strength
    const passwordStrength = calculatePasswordStrength(password);
    const passwordStrengthText = getPasswordStrengthText(passwordStrength);
    const passwordStrengthIndicator = document.getElementById("password-strength-indicator");
    passwordStrengthIndicator.innerText = passwordStrengthText;
    passwordStrengthIndicator.className = `password-strength-indicator ${passwordStrengthText.toLowerCase()}`;
    if (passwordStrength < 2) {
        alert("Password is too weak.");
        return false;
    }

    return true;
}