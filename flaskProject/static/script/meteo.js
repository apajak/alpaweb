// api key = f0c170b5e2cd799a28e5edabbb7e97b4

// 1. Get the current weather data from the API
const api_key = "f0c170b5e2cd799a28e5edabbb7e97b4";
function currentWeather(city) {
    var url = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + api_key;
    // send an HTTP request to the OpenWeatherMap API
    var req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    // parse the JSON response
    var response = JSON.parse(req.responseText);
    // return the current weather data
    return response;
}
function fiveDayForecast(city) {
    var url = "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&appid=" + api_key;
    // send an HTTP request to the OpenWeatherMap API
    var req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    // parse the JSON response
    var response = JSON.parse(req.responseText);
    // return the current weather data
    return response;
}
function toggleForecast() {
    var forecastDiv = document.getElementById("forecast");
    if (forecastDiv.style.display === "none") {
        // If the forecast is currently hidden, show it
        forecastDiv.style.display = "block";
    } else {
        // Otherwise, hide it
        forecastDiv.style.display = "none";
    }
}
