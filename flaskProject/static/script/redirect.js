function redirectToLogin() {
    window.location.href = "/login";
}
function redirectToLogout() {
    window.location.href = "/logout";
}
function redirectToProfile() {
    window.location.href = "/profile";
}
function redirectToAdmin() {
    window.location.href = "/admin";
}
function redirectToHome() {
    window.location.href = "/";
}
function redirectToProjects() {
    window.location.href = "/projects";
}

function redirectToProject(projectId) {
    let url = "/project/" + projectId;
    window.location.href = url;
}