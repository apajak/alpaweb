function initializeDragAndDrop(document) {
    const containers = document.getElementsByClassName('reorder-container');
    let draggingElement = null;
    let interactingWithTextarea = false; // Flag to track textarea interaction

    for (let i = 0; i < containers.length; i++) {
        const container = containers[i];
        const parts = container.getElementsByClassName('part');
        container.addEventListener('dragstart', handleDragStart);
        container.addEventListener('dragover', handleDragOver);
        container.addEventListener('dragenter', handleDragEnter);
        container.addEventListener('dragleave', handleDragLeave);
        container.addEventListener('drop', handleDrop);

        for (let j = 0; j < parts.length; j++) {
            parts[j].setAttribute('draggable', 'true');
        }
    }

    function handleDragStart(event) {
        // Check if the collapsible button is open
        const collapsibleButton = event.target.closest('.part').querySelector('.collapsible');
        if (collapsibleButton && collapsibleButton.classList.contains('active')) {
            event.preventDefault(); // Disable dragging
            return;
        }

        draggingElement = event.target;
        draggingElement.classList.add('dragging');
        event.dataTransfer.effectAllowed = 'move';
        event.dataTransfer.setData('text/html', draggingElement.innerHTML);
    }


    function handleDragOver(event) {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
        return false;
    }

    function handleDragEnter(event) {
        event.preventDefault();
        if (event.target.classList.contains('part')) {
            event.target.classList.add('drop-hover');
        }
        if (event.target.parentNode.classList.contains('part')) {
            event.target.parentNode.classList.add('drop-target');
        }
    }

    function handleDragLeave(event) {
        event.preventDefault();
        if (event.target.classList.contains('part')) {
            event.target.classList.remove('drop-hover');
        }
        if (event.target.parentNode.classList.contains('part')) {
            event.target.parentNode.classList.remove('drop-target');
        }
    }

    function handleDrop(event) {
        if (draggingElement === null) {
            return;
        }
        event.preventDefault();

        if (event.target.classList.contains('part')) {
            event.target.classList.remove('drop-hover');
            const draggingElementIndex = Array.from(draggingElement.parentNode.children).indexOf(draggingElement);
            const dropElementIndex = Array.from(event.target.parentNode.children).indexOf(event.target);

            if (draggingElementIndex < dropElementIndex) {
                event.target.parentNode.insertBefore(draggingElement, event.target.nextSibling);
            } else {
                event.target.parentNode.insertBefore(draggingElement, event.target);
            }
        } else if (event.target.parentNode.classList.contains('part')) {
            event.target.parentNode.classList.remove('drop-hover');
            event.target.parentNode.classList.remove('drop-target');
            const draggingElementIndex = Array.from(draggingElement.parentNode.children).indexOf(draggingElement);
            const dropElementIndex = Array.from(event.target.parentNode.parentNode.children).indexOf(event.target.parentNode);

            if (draggingElementIndex < dropElementIndex) {
                event.target.parentNode.parentNode.insertBefore(draggingElement, event.target.parentNode.nextSibling);
            } else {
                event.target.parentNode.parentNode.insertBefore(draggingElement, event.target.parentNode);
            }
        }

        draggingElement.classList.remove('dragging');
        draggingElement = null;

        const partContainerId = event.target.parentNode.parentNode.id.split('-')[2];
        updatePartIds(partContainerId);
    }
}

