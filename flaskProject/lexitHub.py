import requests
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify, make_response
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from functools import wraps
from werkzeug.utils import secure_filename
from loguru import logger
from mysqlManager import MySQLManager as database
from projectManager import Project, Part
from aboutmeManager import AboutMe, ContactInfo
from routeManager import RedirectionRoutes
from flask_cors import CORS
from gptManager import MDai
from mailManager import PasswordResetEmailSender
import time
from dotenv import load_dotenv
import os
import secrets
import html
import re

mdai = MDai()
app = Flask(__name__, static_url_path='/static')
load_dotenv(".env")
# Set a secret key for the session
app.secret_key = os.getenv("SECRET_KEY")
host = os.getenv("DATABASE_HOST")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
db = database(host, db_user, db_password, db_name)
current_user = None
# Use a constant or configuration file to store the image and movie directories.
IMAGE_DIR = "static/projects/images/"
MOVIE_DIR = "static/projects/movies/"
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'mp4', 'mov', 'avi', 'mkv', 'webm'}
LOG_DIR_ACCESS = "./logs/lexit-cloud-access.log"
LOG_DIR_ERROR = "./logs/lexit-cloud-error.log"
LOG_DIR_SECURITY = "./logs/lexit-cloud-security.log"
LOG_DIR_DEBUG = "./logs/lexit-cloud-debug.log"
LOG_DIR_CRITICAL = "./logs/lexit-cloud-critical.log"
LOG_DIR = "./logs/lexit-cloud.log"
app.config['IMAGE_DIR'] = IMAGE_DIR
app.config['MOVIE_DIR'] = MOVIE_DIR
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS
app.config['MAX_CONTENT_LENGTH'] = 1000 * 1024 * 1024 # 1 GB
MAX_LOGIN_ATTEMPTS = 5
login_attempts = {}
blocked_users = {}
# Define the initial block duration in seconds
initial_block_duration = 60  # 1 minute
# Mail server configuration
smtp_server = os.getenv("MAIL_SERVER")
smtp_port = os.getenv("MAIL_PORT")
smtp_sender = os.getenv("MAIL_USERNAME")
smtp_password = os.getenv("MAIL_PASSWORD")
# Create a password reset email sender
password_reset_email_sender = PasswordResetEmailSender(smtp_server, smtp_port, smtp_sender, smtp_password)
# Set up CORS
# Read the whitelist.txt file
with open('white_list.txt') as f:
    whitelist_urls = f.read().splitlines()

# Configure CORS with the whitelist URLs
CORS(app, resources={r"/*": {"origins": whitelist_urls}})

# Set up rate limiting
limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["100 per second"],
    storage_uri="redis://localhost:6379"
)

# limiter = Limiter(
#     get_remote_address,
#     app=app,
#     default_limits=["100 per second"],
# )

# Set up logging
logger.level("SECURITY-Lv1", no=15, color="<green>")
logger.level("SECURITY-Lv2", no=15, color="<yellow>")
logger.level("SECURITY-Lv3", no=15, color="<red>")
logger.level("ACCESS-Lv1", no=25, color="<green>")
logger.level("ACCESS-Lv2", no=25, color="<blue>")
logger.level("ACCESS-Lv3", no=25, color="<yellow>")
logger.level("DENIED", no=25, color="<red>")

logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv1", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv2", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="ACCESS-Lv3", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ACCESS, level="DENIED", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_ERROR, level="ERROR", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv1", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv2", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_SECURITY, level="SECURITY-Lv3", rotation="1 week", compression="zip", enqueue=True, backtrace=True,
           diagnose=True, colorize=True)
logger.add(LOG_DIR_DEBUG, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True,
           colorize=True)
logger.add(LOG_DIR_CRITICAL, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True,
           colorize=True)
logger.add(LOG_DIR, rotation="1 week", compression="zip", enqueue=True, backtrace=True, diagnose=True, colorize=True)


# Set up error handling
@app.errorhandler(400)
def bad_request(e):
    logger.warning(f"Bad request: {e}")
    logger.log("SECURITY-Lv2", f"Bad request: {e}")
    return render_template('400.html'), 400


@app.errorhandler(403)
def forbidden(e):
    logger.warning(f"Forbidden: {e}")
    logger.log("SECURITY-Lv3", f"Forbidden: {e}")
    return render_template('403.html'), 403


@app.errorhandler(404)
def page_not_found(e):
    logger.warning(f"Page not found: {e}")
    logger.log("SECURITY-Lv1", f"Page not found: {e}")
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404


@app.errorhandler(405)
def method_not_allowed(e):
    logger.warning(f"Method not allowed: {e}")
    logger.log("SECURITY-Lv3", f"Method not allowed: {e}")
    return render_template('405.html'), 405


@app.errorhandler(429)
def ratelimit_handler(e):
    logger.warning(f"Rate limit exceeded: {e}")
    logger.log("SECURITY-Lv3", f"Rate limit exceeded: {e}")
    return render_template('429.html'), 429


@app.errorhandler(500)
def internal_server_error(e):
    logger.critical(f"Internal server error: {e}")
    logger.debug(f"Internal server error: {e}")
    logger.error(f"Internal server error: {e}")
    return render_template('500.html'), 500


@app.errorhandler(502)
def bad_gateway(e):
    logger.critical(f"Bad gateway: {e}")
    logger.debug(f"Bad gateway: {e}")
    logger.error(f"Bad gateway: {e}")
    return render_template('502.html'), 502


@app.errorhandler(503)
def service_unavailable(e):
    logger.critical(f"Service unavailable: {e}")
    logger.debug(f"Service unavailable: {e}")
    return render_template('503.html'), 503


@app.errorhandler(504)
def gateway_timeout(e):
    logger.critical(f"Gateway timeout: {e}")
    logger.debug(f"Gateway timeout: {e}")
    return render_template('504.html'), 504


def login_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.warning(f"User not logged in")
            logger.log("DENIED", f"User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        return route_function(*args, **kwargs)

    return wrapper


def admin_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.warning(f"User not logged in")
            logger.log("DENIED", f"User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        user = db.get_user_by_id(session.get('session'))
        if user.role != "admin":
            logger.warning(f"User {user.username} is not an admin")
            logger.log("DENIED", f"User {user.username} is not an admin")
            logger.log("SECURITY-Lv2", f"User {user.username} is not an admin")
            flash("You must be an admin to access this page", "danger")
            return redirect(url_for('home'))
        return route_function(*args, **kwargs)

    return wrapper


def lexit_required(route_function):
    @wraps(route_function)
    def wrapper(*args, **kwargs):
        if session.get('session') is None:
            logger.log("DENIED", f"User not logged in")
            flash("You must be logged in to access this page", "danger")
            return redirect(url_for('login'))
        user = db.get_user_by_id(session.get('session'))
        if user.id != 4:
            logger.log("DENIED", f"User {user.username} is not LexIt")
            logger.log("SECURITY-Lv3", f"User {user.username} is not LexIt")
            flash("You must be LexIt to access this page", "danger")
            return redirect(url_for('home'))
        return route_function(*args, **kwargs)

    return wrapper


@app.route('/')
def index():
    logger.debug(f"User {session.get('session')} is logged in")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} is logged in")
    if session.get('session') is not None:
        return redirect(url_for('home'))
    redirection = db.get_route()
    try:
        projects_computer = db.get_two_latest_projects_by_type("computer_science")
        projects_robotics = db.get_two_latest_projects_by_type("robotics")
        projects_personal = db.get_two_latest_projects_by_type("personal")
        projects_work = db.get_two_latest_projects_by_type("work")
        projects = projects_computer + projects_robotics + projects_personal + projects_work
        for project in projects:
            project.parts = db.get_parts_by_project_id(project.id)

    except Exception as e:
        logger.error(f"Error getting projects: {e}")
        logger.log("SECURITY-Lv3", f"Error getting projects: {e}")
        projects = []
        flash("Error getting projects", "danger")

    return render_template('index.html', logged_in=False,routes=redirection, projects=projects)


def is_valid_email(email):
    logger.debug(f"Validating email {email}")
    # Use a regular expression or a library like Flask-WTF to validate email format
    email_regex = r'^[\w\.-]+@[\w\.-]+\.\w+$'
    return re.match(email_regex, email)


def sanitize_username_input(input_string):
    logger.debug(f"Sanitizing username input {input_string}")
    sanitized_string = input_string.strip()  # Remove leading/trailing whitespace
    # Additional sanitization steps for the login username
    sanitized_string = sanitized_string[:20]  # Limit the username length to 20 characters
    sanitized_string = re.sub(r'[^a-zA-Z0-9_-]', '',
                              sanitized_string)  # Remove special characters except underscores and hyphens
    return sanitized_string


def sanitize_input(input_string):
    logger.debug(f"Sanitizing input {input_string}")
    sanitized_string = input_string.strip()  # Remove leading/trailing whitespace
    # Additional sanitization steps for the login username
    sanitized_string = sanitized_string[:50]  # Limit the username length to 50 characters
    sanitized_string = re.sub(r'[^a-zA-Z0-9_-]', '',
                              sanitized_string)  # Remove special characters except underscores and hyphens
    return sanitized_string


@app.route('/login', methods=['GET', 'POST'])
@limiter.limit("10/minute, 50/day")
def login():
    user_id = session.get('session')
    if user_id is not None:
        return redirect(url_for('home'))

    # Get the client IP address
    client_ip = request.remote_addr

    # Check if the user is currently blocked
    if blocked_users.get(client_ip, 0) > time.time():
        block_end_time = blocked_users[client_ip]
        remaining_time = int(block_end_time - time.time())
        logger.debug(f"IP address {client_ip} is blocked. Remaining time: {remaining_time} seconds")
        flash(f"You are currently blocked. Please try again in {remaining_time} seconds.", "danger")
        return redirect(url_for('index'))

    if request.method == 'POST':
        identifier = request.form['email']
        password = request.form['password']

        # Check if the client IP has reached the maximum number of login attempts
        if login_attempts.get(client_ip, 0) >= MAX_LOGIN_ATTEMPTS:
            over_attempts_value = 1 + login_attempts.get(client_ip, 0) - MAX_LOGIN_ATTEMPTS
            login_attempts[client_ip] = login_attempts.get(client_ip, 0) + 1
            block_duration = initial_block_duration * over_attempts_value
            block_end_time = time.time() + block_duration
            blocked_users[client_ip] = block_end_time

            logger.debug(f"IP address {client_ip} reached the maximum number of login attempts and is blocked for {block_duration} seconds")
            flash(f"You have reached the maximum number of login attempts. You are blocked for {block_duration} seconds.", "danger")
            return redirect(url_for('index'))

        user = db.login(identifier, password)
        if user is not None:
            session['session'] = user.id
            logger.debug(f"User {session.get('session')} login successful")
            flash("You have been logged in successfully", "success")
            return redirect(url_for('home'))
        else:
            # Increment the login attempts counter for the client IP
            login_attempts[client_ip] = login_attempts.get(client_ip, 0) + 1
            logger.debug(f"IP address {client_ip} login failed ({login_attempts[client_ip]}/{MAX_LOGIN_ATTEMPTS} attempts)")
            flash(f"Wrong email or password. Attempt: {login_attempts[client_ip]}/{MAX_LOGIN_ATTEMPTS}", "danger")
            return redirect(url_for('login'))
    redirections = db.get_route()
    return render_template('login.html', logged_in=False, routes=redirections)

@app.route('/lostPassword', methods=['GET', 'POST'])
def lostPassword():
    # if logged in, redirect to home
    user_id = session.get('session')
    if user_id is not None:
        return redirect(url_for('home'))
    else :
        if request.method == 'POST':
            mail = request.form['email']
            is_valid_email = db.check_email_exists(mail)
            # if email is valid
            if is_valid_email:
                token = secrets.token_urlsafe(16)
                db.add_token(token, mail)
                # send email
                try :
                    password_reset_email_sender.send_password_reset_email(mail, token)
                    flash("Email sent", "success")
                    return redirect(url_for('resetPasswordSend'))
                except :
                    flash("Error sending email", "danger")
                    return redirect(url_for('lostPassword'))
            else :
                flash("Invalid email", "danger")
                return redirect(url_for('lostPassword'))

    redirections = db.get_route()
    return render_template('lostPassword.html', logged_in=False, routes=redirections)

@app.route('/resetPasswordSend', methods=['GET', 'POST'])
def resetPasswordSend():
    # if logged in, redirect to home
    user_id = session.get('session')
    if user_id is not None:
        redirect(url_for('home'))
    else :
        routes = db.get_route()
        return  render_template('resetPasswordSend.html', logged_in=False, routes=routes)


@app.route('/resetPassword', methods=['GET', 'POST'])
def resetPassword():
    # check if url token is valid
    token = request.args.get('token')
    redirections = db.get_route()
    if request.method == 'POST':
        password = request.form['new-password']
        password2 = request.form['confirm-password']
        form_token = request.form['token']
        user = db.get_token(form_token)
        # if passwords match
        if user is not None and password == password2:
            try :
                db.update_user_password(password,user.id)
                db.remove_token(form_token)
                flash("Password updated", "success")
                logger.debug(f"Password updated for user {user.id}")
                logger.log("SECURITY-Lv2", f"Password updated for user {user.id}")
                return redirect(url_for('login'))
            except :
                flash("Error updating password", "danger")
                logger.debug(f"Error updating password for user {user.id}")
                return redirect(url_for('lostPassword'))
        else :
            flash("Invalid token", "danger")
            logger.debug(f"Invalid token")
            logger.log("SECURITY-Lv2", f"Invalid token {form_token} for user {user.id}")
            return redirect(url_for('lostPassword'))
    return render_template('resetPassword.html', logged_in=False, routes=redirections, token=token)


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    user_id = session.get('session')
    user = db.get_user_by_id(user_id)
    if user is None:
        logger.debug(f"User {session.get('session')} is not logged in")
        logger.log("DENIED", f"User {session.get('session')} is not logged in")
        logger.log("SECURITY-Lv2", f"User {session.get('session')} is not logged in for profile")
        return redirect(url_for('login'))
    logger.debug(f"User {user.name} get profile page")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} is logged in")
    if request.method == 'POST':
        # Get form data
        logger.log("SECURITY-Lv2", f"User {session.get('session')} profile post request")
        username = request.form['username']
        email = request.form['email']
        current_password = request.form['current-password']
        password = request.form['password']
        confirm_password = request.form['confirm-password']
        profile_image = request.files['profile-image']

        # Validate current password
        if not db.login(user.email, current_password):
            logger.debug(f"User {session.get('session')} wrong current password")
            logger.log("DENIED", f"User {session.get('session')} wrong current password")
            logger.log("SECURITY-Lv2", f"User {session.get('session')} wrong current password")
            flash("Wrong current password", "danger")
            return redirect(url_for('profile'))

        # Update profile fields
        user.username = sanitize_username_input(username.strip())

        if is_valid_email(email):
            user.email = email.strip()
        else:
            flash("Invalid email", "danger")
            logger.debug(f"User {session.get('session')} invalid email")
            logger.log("DENIED", f"User {session.get('session')} invalid email")
            logger.log("SECURITY-Lv2", f"User {session.get('session')} invalid email")
            return redirect(url_for('profile'))

        if profile_image.filename != '':
            # Validate and handle profile image upload
            allowed_extensions = {'jpg', 'jpeg', 'png', 'gif', 'webp'}
            if '.' in profile_image.filename and profile_image.filename.rsplit('.', 1)[1].lower() in allowed_extensions:
                filename = secure_filename(profile_image.filename)
                profile_image_path = os.path.join("./static/profile_pics", filename)
                profile_image.save(profile_image_path)
                user.image = profile_image_path
            else:
                flash("Invalid profile image file", "danger")
                logger.debug(f"User {session.get('session')} invalid profile image file")
                logger.log("DENIED", f"User {session.get('session')} invalid profile image file")
                logger.log("SECURITY-Lv2", f"User {session.get('session')} invalid profile image file")
                return redirect(url_for('profile'))
        # Update user in the database
        try:
            db.update_user(user.username, user.email, user.image, user.id)
            logger.debug(f"User {session.get('session')} profile updated successfully")
            logger.info(f"User {session.get('session')} profile updated successfully")
        except Exception as e:
            flash("Username or email already exists", "danger")
            logger.debug(f"User {session.get('session')} username or email already exists")
            logger.log("DENIED", f"User {session.get('session')} username or email already exists")
            logger.log("SECURITY-Lv1", f"User {session.get('session')} username or email already exists")
            logger.error(f"User {session.get('session')} username or email already exists: {e}")

            return redirect(url_for('profile'))
        # Update password if provided
        if password.strip() != '':
            if password != confirm_password:
                flash("Passwords don't match", "danger")
                logger.debug(f"User {session.get('session')} passwords don't match")
                logger.log("DENIED", f"User {session.get('session')} passwords don't match")
                return redirect(url_for('profile'))
            else:
                db.update_user_password(password, user_id)
                logger.debug(f"User {session.get('session')} password updated successfully")
                logger.info(f"User {session.get('session')} password updated successfully")
                flash("Password updated successfully", "success")

        flash("Profile updated successfully", "success")
        logger.debug(f"User {session.get('session')} profile updated successfully")
        logger.info(f"User {session.get('session')} profile updated successfully")
        return redirect(url_for('home'))
    redirections = db.get_route()

    return render_template('profile.html', user=user, name=user.name, profile_image_path=user.image,
                           logged_in=True, role=user.role, routes=redirections)


@app.route('/logout')
@login_required
def logout():
    logger.debug(f"User {session.get('session')} logged out")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} logged out")
    session.clear()
    flash("You have been logged out", "info")
    return redirect(url_for('index'))


@app.route('/delete', methods=['POST'])
@admin_required
def delete_user():
    logger.debug(f"User {session.get('session')} delete user")
    logger.log("ACCESS-Lv2", f"User {session.get('session')} delete user")
    logger.log("SECURITY-Lv2", f"User {session.get('session')} delete user")
    if request.method == 'POST':
        user_id = request.form.get('id')
        try:
            user = db.get_user_by_id(user_id)
            if user.id == 4:
                logger.critical(f"User {session.get('session')} tried to delete LexIt")
                logger.log("SECURITY-Lv3", f"User {session.get('session')} tried to delete LexIt")
                flash("You can't delete LexIt. But he can delete you !", "danger")
                return redirect(url_for('admin'))

            db.remove_user_by_id(user_id)
            logger.debug(f"User {user.name} deleted successfully")
            logger.log("ACCESS-Lv2", f"User {user.name} deleted successfully")
            logger.log("SECURITY-Lv1", f"User {user.name} deleted successfully")
            logger.info(f"User {user.name} deleted successfully")
            flash(f"User {user.name} deleted successfully", "success")
            return redirect(url_for('admin'))
        except Exception as e:
            flash("User not deleted", "danger")
            logger.debug(f"User {session.get('session')} user not deleted")
            logger.error(f"User {session.get('session')} user not deleted: {e}")
            return redirect(url_for('admin'))
    redirect(url_for('admin'))


@app.route('/admin', methods=['GET', 'POST'])
@admin_required
def admin():
    if session.get('session') is not None:
        user = db.get_user_by_id(session.get('session'))
        name = user.name
        profile_image = user.image
        loged_in = True
        role = user.role
        if user.role == "admin":
            users = db.get_all_users()
            logger.debug(f"User {session.get('session')} admin page")
            logger.log("ACCESS-Lv3", f"User {session.get('session')} admin page")
            logger.log("SECURITY-Lv2", f"User {session.get('session')} admin page")
            if request.method == 'POST':
                if "user_form" in request.form:
                    id = request.form['id']
                    name = request.form['name']
                    email = request.form['email']
                    password = request.form['password']
                    role = request.form['role']
                    image = request.files['image'] if 'image' in request.files else None
                    if image.filename == "":
                        image_path = None
                    else:
                        logger.debug(f"User {session.get('session')} upload image")
                        logger.log("ACCESS-Lv3", f"User {session.get('session')} upload image")
                        logger.log("SECURITY-Lv2", f"User {session.get('session')} upload image")
                        logger.info(f"User {session.get('session')} upload image")
                        image.save(os.path.join("./static/profile_pics", image.filename))
                        image_path = os.path.join("./static/profile_pics", image.filename)
                    user = db.get_user_by_id(id)
                    if user is not None:
                        if not image_path:
                            image_path = user.image
                        if user.password != password and password != "":
                            try:
                                db.update_user_password(password, id)
                                logger.info(f"User {session.get('session')} password updated successfully")
                                logger.debug(f"User {session.get('session')} password updated successfully")
                                logger.log("ACCESS-Lv3", f"User {session.get('session')} password updated successfully")
                                logger.log("SECURITY-Lv2", f"User {session.get('session')} password updated successfully")
                                flash("Password updated successfully", "success")
                            except Exception as e:
                                logger.error(f"User {session.get('session')} password not updated: {e}")
                                logger.debug(f"User {session.get('session')} password not updated")
                                logger.log("DENNIED", f"User {session.get('session')} password not updated")
                                flash("Password not updated", "danger")
                        try:
                            logger.debug(f"User {session.get('session')} update user")
                            logger.log("ACCESS-Lv3", f"User {session.get('session')} update user")
                            logger.log("SECURITY-Lv2", f"User {session.get('session')} update user")
                            logger.info(f"User {session.get('session')} update user")
                            db.update_user(username=name, email=email, image=image_path, id=id)
                            db.update_user_role(role, id)
                            flash("User updated successfully", "success")
                        except Exception as e:
                            logger.error(f"User {session.get('session')} user not updated: {e}")
                            logger.debug(f"User {session.get('session')} user not updated")
                            logger.log("DENNIED", f"User {session.get('session')} user not updated")
                            flash("User not updated", "danger")
                    else:
                        logger.error(f"User {session.get('session')} user not found")
                        logger.debug(f"User {session.get('session')} user not found")
                        logger.log("DENNIED", f"User {session.get('session')} user not found")
                        flash("User not found", "danger")
                    return redirect(url_for('admin'))

                elif "about_me_form" in request.form:
                    about_me = db.get_about_me()


                    about_me.firstname = request.form['firstname']
                    about_me.lastname = request.form['lastname']
                    about_me.pseudo = request.form['pseudo']
                    about_me.date_of_birth = request.form['date_of_birth']
                    about_me.introduction = request.form['introduction']
                    about_me.background = request.form['background']
                    about_me.experience = request.form['experience']


                    hard_skills = about_me.hard_skills
                    # add hard skills
                    added_hard_skills = request.form.getlist('hard_skills-added_main')[0]
                    if added_hard_skills:
                        for skill in added_hard_skills.split(",")[:-1]:
                            hard_skills.append(skill)
                    # remove hard skills
                    removed_hard_skills = request.form.getlist('hard_skills-removed_main')[0]
                    if removed_hard_skills:
                        for skill in removed_hard_skills.split(",")[:-1]:
                            hard_skills.remove(skill)
                    about_me.hard_skills = hard_skills

                    # hard_skills OS:
                    hard_skills_os = about_me.hard_skills_os
                    added_hard_skills_os = request.form.getlist('hard_skills-added_os')[0]
                    if added_hard_skills_os:
                        for skill in added_hard_skills_os.split(",")[:-1]:
                            if skill not in hard_skills_os:
                                hard_skills_os.append(skill)
                    remouved_hard_skills_os = request.form.getlist('hard_skills-removed_os')[0]
                    if remouved_hard_skills_os:
                        for skill in remouved_hard_skills_os.split(",")[:-1]:
                            if skill in hard_skills_os:
                                hard_skills_os.remove(skill)
                    about_me.hard_skills_os = hard_skills_os

                    # hard_skills OS highlights:
                    hard_skills_os_highlights = about_me.hard_skills_os_highlights
                    added_hard_skills_os_highlights = request.form.getlist('hard_skills-added_os_highlights')[0]
                    remouved_hard_skills_os_highlights = request.form.getlist('hard_skills-removed_os_highlights')[0]
                    if added_hard_skills_os_highlights:
                        for skill in added_hard_skills_os_highlights.split(",")[:-1]:
                            if skill not in hard_skills_os_highlights:
                                hard_skills_os_highlights.append(skill)
                    if remouved_hard_skills_os_highlights:
                        for skill in remouved_hard_skills_os_highlights.split(",")[:-1]:
                            if skill in hard_skills_os_highlights:
                                hard_skills_os_highlights.remove(skill)
                    about_me.hard_skills_os_highlights = hard_skills_os_highlights



                    # hard_skills infra:
                    hard_skills_infra = about_me.hard_skills_infra
                    added_hard_skills_infra = request.form.getlist('hard_skills-added_infra')[0]
                    removed_hard_skills_infra = request.form.getlist('hard_skills-removed_infra')[0]
                    if added_hard_skills_infra:
                        for skill in added_hard_skills_infra.split(",")[:-1]:
                            if skill not in hard_skills_infra:
                                hard_skills_infra.append(skill)
                    if removed_hard_skills_infra:
                        for skill in removed_hard_skills_infra.split(",")[:-1]:
                            if skill in hard_skills_infra:
                                hard_skills_infra.remove(skill)
                    about_me.hard_skills_infra = hard_skills_infra

                    hard_skills_infra_highlights = about_me.hard_skills_infra_highlights
                    added_hard_skills_infra_highlights = request.form.getlist('hard_skills-added_infra_highlights')[0]
                    remouved_hard_skills_infra_highlights = request.form.getlist('hard_skills-removed_infra_highlights')[0]
                    if added_hard_skills_infra_highlights:
                        for skill in added_hard_skills_infra_highlights.split(",")[:-1]:
                            if skill not in hard_skills_infra_highlights:
                                hard_skills_infra_highlights.append(skill)
                    if remouved_hard_skills_infra_highlights:
                        for skill in remouved_hard_skills_infra_highlights.split(",")[:-1]:
                            if skill in hard_skills_infra_highlights:
                                hard_skills_infra_highlights.remove(skill)
                    about_me.hard_skills_infra_highlights = hard_skills_infra_highlights

                    # hard_skills Network:
                    hard_skills_network = about_me.hard_skills_network
                    added_hard_skills_network = request.form.getlist('hard_skills-added_network')[0]
                    remouved_hard_skills_network = request.form.getlist('hard_skills-removed_network')[0]
                    if added_hard_skills_network:
                        for skill in added_hard_skills_network.split(",")[:-1]:
                            if skill not in hard_skills_network:
                                hard_skills_network.append(skill)
                    if remouved_hard_skills_network:
                        for skill in remouved_hard_skills_network.split(",")[:-1]:
                            if skill in hard_skills_network:
                                hard_skills_network.remove(skill)
                    about_me.hard_skills_network = hard_skills_network

                    hard_skills_network_highlights = about_me.hard_skills_network_highlights
                    added_hard_skills_network_highlights = request.form.getlist('hard_skills-added_network_highlights')[0]
                    remouved_hard_skills_network_highlights = request.form.getlist('hard_skills-removed_network_highlights')[0]
                    if added_hard_skills_network_highlights:
                        for skill in added_hard_skills_network_highlights.split(",")[:-1]:
                            if skill not in hard_skills_network_highlights:
                                hard_skills_network_highlights.append(skill)
                    if remouved_hard_skills_network_highlights:
                        for skill in remouved_hard_skills_network_highlights.split(",")[:-1]:
                            if skill in hard_skills_network_highlights:
                                hard_skills_network_highlights.remove(skill)
                    about_me.hard_skills_network_highlights = hard_skills_network_highlights

                    # hard_skills Web:
                    hard_skills_web = about_me.hard_skills_web
                    added_hard_skills_web = request.form.getlist('hard_skills-added_web')[0]
                    remouved_hard_skills_web = request.form.getlist('hard_skills-removed_web')[0]
                    if added_hard_skills_web:
                        for skill in added_hard_skills_web.split(",")[:-1]:
                            if skill not in hard_skills_web:
                                hard_skills_web.append(skill)
                    if remouved_hard_skills_web:
                        for skill in remouved_hard_skills_web.split(",")[:-1]:
                            if skill in hard_skills_web:
                                hard_skills_web.remove(skill)
                    about_me.hard_skills_web = hard_skills_web

                    hard_skills_web_highlights = about_me.hard_skills_web_highlights
                    added_hard_skills_web_highlights = request.form.getlist('hard_skills-added_web_highlights')[0]
                    remouved_hard_skills_web_highlights = request.form.getlist('hard_skills-removed_web_highlights')[0]
                    if added_hard_skills_web_highlights:
                        for skill in added_hard_skills_web_highlights.split(",")[:-1]:
                            if skill not in hard_skills_web_highlights:
                                hard_skills_web_highlights.append(skill)
                    if remouved_hard_skills_web_highlights:
                        for skill in remouved_hard_skills_web_highlights.split(",")[:-1]:
                            if skill in hard_skills_web_highlights:
                                hard_skills_web_highlights.remove(skill)
                    about_me.hard_skills_web_highlights = hard_skills_web_highlights

                    # hard_skills Mobile:
                    hard_skills_mobile = about_me.hard_skills_mobile
                    added_hard_skills_mobile = request.form.getlist('hard_skills-added_mobile')[0]
                    removed_hard_skills_mobile = request.form.getlist('hard_skills-removed_mobile')[0]
                    if added_hard_skills_mobile:
                        for skill in added_hard_skills_mobile.split(",")[:-1]:
                            if skill not in hard_skills_mobile:
                                hard_skills_mobile.append(skill)
                    if removed_hard_skills_mobile:
                        for skill in removed_hard_skills_mobile.split(",")[:-1]:
                            if skill in hard_skills_mobile:
                                hard_skills_mobile.remove(skill)
                    about_me.hard_skills_mobile = hard_skills_mobile

                    hard_skills_mobile_highlights = about_me.hard_skills_mobile_highlights
                    added_hard_skills_mobile_highlights = request.form.getlist('hard_skills-added_mobile_highlights')[0]
                    removed_hard_skills_mobile_highlights = request.form.getlist('hard_skills-removed_mobile_highlights')[0]
                    if added_hard_skills_mobile_highlights:
                        for skill in added_hard_skills_mobile_highlights.split(",")[:-1]:
                            if skill not in hard_skills_mobile_highlights:
                                hard_skills_mobile_highlights.append(skill)
                    if removed_hard_skills_mobile_highlights:
                        for skill in removed_hard_skills_mobile_highlights.split(",")[:-1]:
                            if skill in hard_skills_mobile_highlights:
                                hard_skills_mobile_highlights.remove(skill)
                    about_me.hard_skills_mobile_highlights = hard_skills_mobile_highlights

                    # hard_skills AI:
                    hard_skills_ai = about_me.hard_skills_ai
                    added_hard_skills_ai = request.form.getlist('hard_skills-added_ai')[0]
                    remouved_hard_skills_ai = request.form.getlist('hard_skills-removed_ai')[0]
                    if added_hard_skills_ai:
                        for skill in added_hard_skills_ai.split(",")[:-1]:
                            if skill not in hard_skills_ai:
                                hard_skills_ai.append(skill)
                    if remouved_hard_skills_ai:
                        for skill in remouved_hard_skills_ai.split(",")[:-1]:
                            if skill in hard_skills_ai:
                                hard_skills_ai.remove(skill)
                    about_me.hard_skills_ai = hard_skills_ai

                    hard_skills_ai_highlights = about_me.hard_skills_ai_highlights
                    added_hard_skills_ai_highlights = request.form.getlist('hard_skills-added_ai_highlights')[0]
                    remouved_hard_skills_ai_highlights = request.form.getlist('hard_skills-removed_ai_highlights')[0]
                    if added_hard_skills_ai_highlights:
                        for skill in added_hard_skills_ai_highlights.split(",")[:-1]:
                            if skill not in hard_skills_ai_highlights:
                                hard_skills_ai_highlights.append(skill)
                    if remouved_hard_skills_ai_highlights:
                        for skill in remouved_hard_skills_ai_highlights.split(",")[:-1]:
                            if skill in hard_skills_ai_highlights:
                                hard_skills_ai_highlights.remove(skill)
                    about_me.hard_skills_ai_highlights = hard_skills_ai_highlights

                    # hard_skills developpement:
                    hard_skills_developpement = about_me.hard_skills_dev
                    added_hard_skills_developpement = request.form.getlist('hard_skills-added_developpement')[0]
                    remouved_hard_skills_developpement = request.form.getlist('hard_skills-removed_developpement')[0]
                    if added_hard_skills_developpement:
                        for skill in added_hard_skills_developpement.split(",")[:-1]:
                            if skill not in hard_skills_developpement:
                                hard_skills_developpement.append(skill)
                    if remouved_hard_skills_developpement:
                        for skill in remouved_hard_skills_developpement.split(",")[:-1]:
                            if skill in hard_skills_developpement:
                                hard_skills_developpement.remove(skill)
                    about_me.hard_skills_dev = hard_skills_developpement

                    hard_skills_developpement_highlights = about_me.hard_skills_dev_highlights
                    added_hard_skills_developpement_highlights = request.form.getlist('hard_skills-added_developpement_highlights')[0]
                    remouved_hard_skills_developpement_highlights = request.form.getlist('hard_skills-removed_developpement_highlights')[0]
                    if added_hard_skills_developpement_highlights:
                        for skill in added_hard_skills_developpement_highlights.split(",")[:-1]:
                            if skill not in hard_skills_developpement_highlights:
                                hard_skills_developpement_highlights.append(skill)
                    if remouved_hard_skills_developpement_highlights:
                        for skill in remouved_hard_skills_developpement_highlights.split(",")[:-1]:
                            if skill in hard_skills_developpement_highlights:
                                hard_skills_developpement_highlights.remove(skill)
                    about_me.hard_skills_dev_highlights = hard_skills_developpement_highlights

                    # hard_skills Data:
                    hard_skills_data = about_me.hard_skills_data
                    added_hard_skills_data = request.form.getlist('hard_skills-added_data')[0]
                    remouved_hard_skills_data = request.form.getlist('hard_skills-removed_data')[0]
                    if added_hard_skills_data:
                        for skill in added_hard_skills_data.split(",")[:-1]:
                            if skill not in hard_skills_data:
                                hard_skills_data.append(skill)
                    if remouved_hard_skills_data:
                        for skill in remouved_hard_skills_data.split(",")[:-1]:
                            if skill in hard_skills_data:
                                hard_skills_data.remove(skill)
                    about_me.hard_skills_data = hard_skills_data

                    hard_skills_data_highlights = about_me.hard_skills_data_highlights
                    added_hard_skills_data_highlights = request.form.getlist('hard_skills-added_data_highlights')[0]
                    removed_hard_skills_data_highlights = request.form.getlist('hard_skills-removed_data_highlights')[0]
                    if added_hard_skills_data_highlights:
                        for skill in added_hard_skills_data_highlights.split(",")[:-1]:
                            if skill not in hard_skills_data_highlights:
                                hard_skills_data_highlights.append(skill)
                    if removed_hard_skills_data_highlights:
                        for skill in removed_hard_skills_data_highlights.split(",")[:-1]:
                            if skill in hard_skills_data_highlights:
                                hard_skills_data_highlights.remove(skill)
                    about_me.hard_skills_data_highlights = hard_skills_data_highlights

                    # hard_skills Cloud:
                    hard_skills_cloud = about_me.hard_skills_cloud
                    added_hard_skills_cloud = request.form.getlist('hard_skills-added_cloud')[0]
                    removed_hard_skills_cloud = request.form.getlist('hard_skills-removed_cloud')[0]
                    if added_hard_skills_cloud:
                        for skill in added_hard_skills_cloud.split(",")[:-1]:
                            if skill not in hard_skills_cloud:
                                hard_skills_cloud.append(skill)
                    if removed_hard_skills_cloud:
                        for skill in removed_hard_skills_cloud.split(",")[:-1]:
                            if skill in hard_skills_cloud:
                                hard_skills_cloud.remove(skill)
                    about_me.hard_skills_cloud = hard_skills_cloud

                    hard_skills_cloud_highlights = about_me.hard_skills_cloud_highlights
                    added_hard_skills_cloud_highlights = request.form.getlist('hard_skills-added_cloud_highlights')[0]
                    removed_hard_skills_cloud_highlights = request.form.getlist('hard_skills-removed_cloud_highlights')[0]
                    if added_hard_skills_cloud_highlights:
                        for skill in added_hard_skills_cloud_highlights.split(",")[:-1]:
                            if skill not in hard_skills_cloud_highlights:
                                hard_skills_cloud_highlights.append(skill)
                    if removed_hard_skills_cloud_highlights:
                        for skill in removed_hard_skills_cloud_highlights.split(",")[:-1]:
                            if skill in hard_skills_cloud_highlights:
                                hard_skills_cloud_highlights.remove(skill)
                    about_me.hard_skills_cloud_highlights = hard_skills_cloud_highlights

                    # hard_skills Security:
                    hard_skills_security = about_me.hard_skills_security
                    added_hard_skills_security = request.form.getlist('hard_skills-added_security')[0]
                    removed_hard_skills_security = request.form.getlist('hard_skills-removed_security')[0]
                    if added_hard_skills_security:
                        for skill in added_hard_skills_security.split(",")[:-1]:
                            if skill not in hard_skills_security:
                                hard_skills_security.append(skill)
                    if removed_hard_skills_security:
                        for skill in removed_hard_skills_security.split(",")[:-1]:
                            if skill in hard_skills_security:
                                hard_skills_security.remove(skill)
                    about_me.hard_skills_security = hard_skills_security

                    hard_skills_security_highlights = about_me.hard_skills_security_highlights
                    added_hard_skills_security_highlights = request.form.getlist('hard_skills-added_security_highlights')[0]
                    removed_hard_skills_security_highlights = request.form.getlist('hard_skills-removed_security_highlights')[0]
                    if added_hard_skills_security_highlights:
                        for skill in added_hard_skills_security_highlights.split(",")[:-1]:
                            if skill not in hard_skills_security_highlights:
                                hard_skills_security_highlights.append(skill)
                    if removed_hard_skills_security_highlights:
                        for skill in removed_hard_skills_security_highlights.split(",")[:-1]:
                            if skill in hard_skills_security_highlights:
                                hard_skills_security_highlights.remove(skill)
                    about_me.hard_skills_security_highlights = hard_skills_security_highlights

                    # hard_skills Electronic:
                    hard_skills_electronic = about_me.hard_skills_electronic
                    added_hard_skills_electronic = request.form.getlist('hard_skills-added_electronic')[0]
                    removed_hard_skills_electronic = request.form.getlist('hard_skills-removed_electronic')[0]
                    if added_hard_skills_electronic:
                        for skill in added_hard_skills_electronic.split(",")[:-1]:
                            if skill not in hard_skills_electronic:
                                hard_skills_electronic.append(skill)
                    if removed_hard_skills_electronic:
                        for skill in removed_hard_skills_electronic.split(",")[:-1]:
                            if skill in hard_skills_electronic:
                                hard_skills_electronic.remove(skill)
                    about_me.hard_skills_electronic = hard_skills_electronic

                    hard_skills_electronic_highlights = about_me.hard_skills_electronic_highlights
                    added_hard_skills_electronic_highlights = request.form.getlist('hard_skills-added_electronic_highlights')[0]
                    removed_hard_skills_electronic_highlights = request.form.getlist('hard_skills-removed_electronic_highlights')[0]
                    if added_hard_skills_electronic_highlights:
                        for skill in added_hard_skills_electronic_highlights.split(",")[:-1]:
                            if skill not in hard_skills_electronic_highlights:
                                hard_skills_electronic_highlights.append(skill)
                    if removed_hard_skills_electronic_highlights:
                        for skill in removed_hard_skills_electronic_highlights.split(",")[:-1]:
                            if skill in hard_skills_electronic_highlights:
                                hard_skills_electronic_highlights.remove(skill)
                    about_me.hard_skills_electronic_highlights = hard_skills_electronic_highlights

                    # hard_skills mechatronics:
                    hard_skills_mechatronics = about_me.hard_skills_mechatronics
                    added_hard_skills_mechatronics = request.form.getlist('hard_skills-added_mechatronics')[0]
                    removed_hard_skills_mechatronics = request.form.getlist('hard_skills-removed_mechatronics')[0]
                    if added_hard_skills_mechatronics:
                        for skill in added_hard_skills_mechatronics.split(",")[:-1]:
                            if skill not in hard_skills_mechatronics:
                                hard_skills_mechatronics.append(skill)
                    if removed_hard_skills_mechatronics:
                        for skill in removed_hard_skills_mechatronics.split(",")[:-1]:
                            if skill in hard_skills_mechatronics:
                                hard_skills_mechatronics.remove(skill)
                    about_me.hard_skills_mechatronics = hard_skills_mechatronics

                    hard_skills_mechatronics_highlights = about_me.hard_skills_mechatronics_highlights
                    added_hard_skills_mechatronics_highlights = request.form.getlist('hard_skills-added_mechatronics_highlights')[0]
                    removed_hard_skills_mechatronics_highlights = request.form.getlist('hard_skills-removed_mechatronics_highlights')[0]
                    if added_hard_skills_mechatronics_highlights:
                        for skill in added_hard_skills_mechatronics_highlights.split(",")[:-1]:
                            if skill not in hard_skills_mechatronics_highlights:
                                hard_skills_mechatronics_highlights.append(skill)
                    if removed_hard_skills_mechatronics_highlights:
                        for skill in removed_hard_skills_mechatronics_highlights.split(",")[:-1]:
                            if skill in hard_skills_mechatronics_highlights:
                                hard_skills_mechatronics_highlights.remove(skill)
                    about_me.hard_skills_mechatronics_highlights = hard_skills_mechatronics_highlights

                    # hard_skills embedded_dev:
                    hard_skills_embedded_dev = about_me.hard_skills_embedded_dev
                    added_hard_skills_embedded_dev = request.form.getlist('hard_skills-added_embedded_dev')[0]
                    removed_hard_skills_embedded_dev = request.form.getlist('hard_skills-removed_embedded_dev')[0]
                    if added_hard_skills_embedded_dev:
                        for skill in added_hard_skills_embedded_dev.split(",")[:-1]:
                            if skill not in hard_skills_embedded_dev:
                                hard_skills_embedded_dev.append(skill)
                    if removed_hard_skills_embedded_dev:
                        for skill in removed_hard_skills_embedded_dev.split(",")[:-1]:
                            if skill in hard_skills_embedded_dev:
                                hard_skills_embedded_dev.remove(skill)
                    about_me.hard_skills_embedded_dev = hard_skills_embedded_dev

                    hard_skills_embedded_dev_highlights = about_me.hard_skills_embedded_dev_highlights
                    added_hard_skills_embedded_dev_highlights = request.form.getlist('hard_skills-added_embedded_dev_highlights')[0]
                    removed_hard_skills_embedded_dev_highlights = request.form.getlist('hard_skills-removed_embedded_dev_highlights')[0]
                    if added_hard_skills_embedded_dev_highlights:
                        for skill in added_hard_skills_embedded_dev_highlights.split(",")[:-1]:
                            if skill not in hard_skills_embedded_dev_highlights:
                                hard_skills_embedded_dev_highlights.append(skill)
                    if removed_hard_skills_embedded_dev_highlights:
                        for skill in removed_hard_skills_embedded_dev_highlights.split(",")[:-1]:
                            if skill in hard_skills_embedded_dev_highlights:
                                hard_skills_embedded_dev_highlights.remove(skill)
                    about_me.hard_skills_embedded_dev_highlights = hard_skills_embedded_dev_highlights

                    # hard_skills other:
                    hard_skills_other = about_me.hard_skills_other
                    added_hard_skills_other = request.form.getlist('hard_skills-added_other')[0]
                    removed_hard_skills_other = request.form.getlist('hard_skills-removed_other')[0]
                    if added_hard_skills_other:
                        for skill in added_hard_skills_other.split(",")[:-1]:
                            if skill not in hard_skills_other:
                                hard_skills_other.append(skill)
                    if removed_hard_skills_other:
                        for skill in removed_hard_skills_other.split(",")[:-1]:
                            if skill in hard_skills_other:
                                hard_skills_other.remove(skill)
                    about_me.hard_skills_other = hard_skills_other

                    hard_skills_other_highlights = about_me.hard_skills_other_highlights
                    added_hard_skills_other_highlights = request.form.getlist('hard_skills-added_other_highlights')[0]
                    removed_hard_skills_other_highlights = request.form.getlist('hard_skills-removed_other_highlights')[0]
                    if added_hard_skills_other_highlights:
                        for skill in added_hard_skills_other_highlights.split(",")[:-1]:
                            if skill not in hard_skills_other_highlights:
                                hard_skills_other_highlights.append(skill)
                    if removed_hard_skills_other_highlights:
                        for skill in removed_hard_skills_other_highlights.split(",")[:-1]:
                            if skill in hard_skills_other_highlights:
                                hard_skills_other_highlights.remove(skill)
                    about_me.hard_skills_other_highlights = hard_skills_other_highlights

                    # soft_skills:
                    soft_skills = about_me.soft_skills
                    added_soft_skills = request.form.getlist('soft_skills-added')[0]
                    removed_soft_skills = request.form.getlist('soft_skills-removed')[0]
                    if added_soft_skills:
                        for skill in added_soft_skills.split(",")[:-1]:
                            if skill not in soft_skills:
                                soft_skills.append(skill)
                    if removed_soft_skills:
                        for skill in removed_soft_skills.split(",")[:-1]:
                            if skill in soft_skills:
                                soft_skills.remove(skill)
                    about_me.soft_skills = soft_skills

                    # soft_skills work:
                    soft_skills_work = about_me.soft_skills_work
                    added_soft_skills_work = request.form.getlist('soft_skills-added-work')[0]
                    removed_soft_skills_work = request.form.getlist('soft_skills-removed-work')[0]
                    if added_soft_skills_work:
                        for skill in added_soft_skills_work.split(",")[:-1]:
                            if skill not in soft_skills_work:
                                soft_skills_work.append(skill)
                    if removed_soft_skills_work:
                        for skill in removed_soft_skills_work.split(",")[:-1]:
                            if skill in soft_skills_work:
                                soft_skills_work.remove(skill)
                    about_me.soft_skills_work = soft_skills_work

                    # languages:
                    languages = about_me.languages
                    languages_added = request.form.getlist('languages-added')[0]
                    languages_removed = request.form.getlist('languages-removed')[0]
                    if languages_added:
                        for language in languages_added.split(",")[:-1]:
                            if language not in languages:
                                languages.append(language)
                    if languages_removed:
                        for language in languages_removed.split(",")[:-1]:
                            if language in languages:
                                languages.remove(language)
                    about_me.languages = languages

                    # achievements:
                    achievements = about_me.achievements
                    achievements_added = request.form.getlist('achievements-added')[0]
                    achievements_removed = request.form.getlist('achievements-removed')[0]
                    if achievements_added:
                        for achievement in achievements_added.split(",")[:-1]:
                            if achievement not in achievements:
                                achievements.append(achievement)
                    if achievements_removed:
                        for achievement in achievements_removed.split(",")[:-1]:
                            if achievement in achievements:
                                achievements.remove(achievement)
                    about_me.achievements = achievements

                    # testimonials:
                    testimonials = about_me.testimonials
                    testimonials_author = about_me.testimonials_author
                    testimonials_added = request.form.getlist('testimonials-added')[0]
                    print(testimonials_added)
                    testimonials_added_author = request.form.getlist('testimonials-added-authors')[0]
                    print(testimonials_added_author)

                    # add testimonials
                    if testimonials_added:
                        for testimonial, author in zip(testimonials_added.split(";")[:-1], testimonials_added_author.split(";")[:-1]):
                            if testimonial not in testimonials:
                                print(testimonial)
                                testimonials.append(testimonial)
                                testimonials_author.append(author)

                    # remove testimonials
                    testimonials_removed = request.form.getlist('testimonials-removed')[0]
                    testimonials_removed_author = request.form.getlist('testimonial-authors-removed')[0]

                    if testimonials_removed:
                        for testimonial, author in zip(testimonials_removed.split(";")[:-1], testimonials_removed_author.split(";")[:-1]):
                            if testimonial in testimonials:
                                # get index of testimonial to remove
                                index = testimonials.index(testimonial)
                                testimonials.remove(testimonial)
                                testimonials_author.pop(index)
                    about_me.testimonials = testimonials
                    about_me.testimonials_author = testimonials_author

                    # hobbies:
                    hobbies = about_me.hobbies
                    hobbies_added = request.form.getlist('hobbies-added')[0]
                    hobbies_removed = request.form.getlist('hobbies-removed')[0]
                    if hobbies_added:
                        for hobby in hobbies_added.split(",")[:-1]:
                            if hobby not in hobbies:
                                hobbies.append(hobby)
                    if hobbies_removed:
                        for hobby in hobbies_removed.split(",")[:-1]:
                            if hobby in hobbies:
                                hobbies.remove(hobby)
                    about_me.hobbies = hobbies

                    about_me.target_job = request.form.get('target_job')


                    contact_info = ContactInfo(
                        personal_email=request.form.get('personal_email'),
                        lexit_email=request.form.get('lexit_email'),
                        school_email=request.form.get('school_email'),
                        phone=request.form.get('phone'),
                        address=request.form.get('address')
                    )
                    try :
                        about_me.contact_info = contact_info
                        db.update_about_me(about_me)
                        logger.info(f"About me updated")
                        flash("About me updated", "success")
                    except Exception as e:
                        logger.error(f"Error updating about me: {e}")
                        flash("Error updating about me", "danger")

                    try :
                        db.update_contact_info(contact_info)
                        logger.info(f"Contact info updated")
                        flash("Contact info updated", "success")
                    except Exception as e:
                        logger.error(f"Error updating contact info: {e}")
                        flash("Error updating contact info", "danger")

                elif "redirect" in request.form:
                    personal_gitlab = request.form.get('personal-gitlab')
                    cloud_arpanode = request.form.get('cloud-arpanode')
                    mattermost_arpanode = request.form.get('mattermost-arpanode')
                    markdownify = request.form.get('markdownify-arpanode')
                    artemis = request.form.get('artemis-arpanode')
                    monitoring = request.form.get('monitoring')
                    tensorboard = request.form.get('tensorboard')
                    trello = request.form.get('trello')
                    extranet = request.form.get('extranet')
                    hyperplanning = request.form.get('hyperplanning')
                    ydays = request.form.get('ydays')
                    ymatch = request.form.get('ymatch')
                    outlook = request.form.get('outlook')
                    gitlab = request.form.get('gitlab')
                    linkedin = request.form.get('linkedin')
                    instagram = request.form.get('instagram')

                    routes = RedirectionRoutes(
                        personal_gitlab=personal_gitlab,
                        cloud_arpanode=cloud_arpanode,
                        mattermost_arpanode=mattermost_arpanode,
                        monitoring=monitoring,
                        tensorboard=tensorboard,
                        trello=trello,
                        extranet=extranet,
                        hyperplanning=hyperplanning,
                        ydays=ydays,
                        ymatch=ymatch,
                        outlook=outlook,
                        gitlab=gitlab,
                        linkedin=linkedin,
                        instagram=instagram,
                        markdownify=markdownify,
                        artemis=artemis
                    )
                    try :
                        db.update_route(routes)
                        logger.info(f"Redirection routes updated")
                        flash("Redirection routes updated", "success")
                    except Exception as e:
                        logger.error(f"Error updating redirection routes: {e}")
                        flash("Error updating redirection routes", "danger")

                if request.files:
                    resume = request.files['resume']
                    if resume.filename == "":
                        flash("No resume uploaded", "info")
                        return redirect(url_for('admin'))
                    else:
                        logger.debug(f"User {session.get('session')} upload resume")
                        logger.log("ACCESS-Lv3", f"User {session.get('session')} upload resume")
                        logger.log("SECURITY-Lv2", f"User {session.get('session')} upload resume")
                        logger.info(f"User {session.get('session')} upload resume")
                        # remove all files in the folder
                        for file in os.listdir("./static/files/resume"):
                            os.remove(os.path.join("./static/files/resume", file))
                        try :
                            about_me = db.get_about_me()
                            path = os.path.join("./static/files/resume", resume.filename)
                            resume.save(path)
                            about_me.resume = path
                            db.update_about_me(about_me)
                            flash("Resume uploaded successfully", "success")
                        except Exception as e:
                            logger.error(f"Error uploading resume: {e}")
                            flash("Error uploading resume", "danger")



            about_me = db.get_about_me()
            redirections = db.get_route()
            about_me.contact_info = db.get_contact_info()
            zipped_testimonials = zip(about_me.testimonials_author, about_me.testimonials)

            return render_template('admin.html', users=users, logged_in=True, name=name,
                                   profile_image_path=profile_image, role=role, about_me=about_me,testimonials=zipped_testimonials,routes=redirections)
        else:
            logger.error(f"User {session.get('session')} tried to access admin page")
            logger.debug(f"User {session.get('session')} tried to access admin page")
            logger.log("DENNIED", f"User {session.get('session')} tried to access admin page")
            logger.log("SECURITY-Lv3", f"User {session.get('session')} tried to access admin page")
            flash("You are not admin", "danger")
            return redirect(url_for('home'))
    else:
        return redirect(url_for('login'))


# Route for fetching a user's information
@app.route('/users/<int:user_id>', methods=['GET'])
@admin_required
def get_user(user_id):
    logger.debug(f"User {session.get('session')} get user")
    logger.log("ACCESS-Lv3", f"User {session.get('session')} get user")
    logger.log("SECURITY-Lv3", f"User {session.get('session')} get user")
    logger.info(f"User {session.get('session')} get user")
    user = db.get_user_by_id(user_id)
    if user:
        return user.json()
    else:
        return jsonify({"error": "User not found"})


@app.route('/register', methods=['GET', 'POST'])
def register():
    logger.debug(f"User register")
    logger.log("ACCESS-Lv1", f"User register")
    logger.log("SECURITY-Lv1", f"User register")
    logger.info(f"User register")
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        loged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        loged_in = False
        role = None

    if request.method == 'POST':
        # Get form data
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        confirm_password = request.form['confirm-password']
        profile_image = request.files['profile-image']

        if password != confirm_password:
            flash("Passwords don't match", "danger")
            return redirect(url_for('register'))

        if user_check(username, email):
            # store it in /static/profile_pics
            profile_image.save(os.path.join("./static/profile_pics", profile_image.filename))
            image_path = os.path.join("./static/profile_pics", profile_image.filename)
            logger.log("SECURITY-Lv2", f"User {session.get('session')} upload image")
            logger.info(f"User {session.get('session')} upload image")
            logger.debug(f"User {session.get('session')} upload image")
            # Insert user data into database
            db.register(username, password, email, image_path)
            flash("You have been registered successfully", "success")
            return redirect(url_for('login'))
        else:
            logger.error(f"User {session.get('session')} username or email already taken")
            logger.debug(f"User {session.get('session')} username or email already taken")
            logger.log("DENNIED", f"User {session.get('session')} username or email already taken")
            flash("Username or email already taken", "danger")
            return redirect(url_for('register'))
    redirections = db.get_route()
    return render_template('register.html', name=name, profile_image_path=profile_image, logged_in=loged_in, role=role,routes=redirections)


def user_check(username, email):
    logger.debug(f"check user: {username} and email: {email}")
    # check by regex if username is valid (only letters and numbers more than 3 characters and less than 20)
    if not re.match("^[a-zA-Z0-9]{3,20}$", username):
        return False
    # check by regex if email is valid
    if not re.match("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", email):
        return False
    # check if username is already taken
    if db.check_user_exists(username, email):
        return False
    return True


def question_sanitizer(question):
    logger.debug(f"Sanitize question: {question}")
    # Remove leading/trailing white spaces
    sanitized_question = question.strip()

    # Remove any HTML tags and escape special characters
    sanitized_question = html.escape(re.sub('<.*?>', '', sanitized_question))

    # Remove any non-ASCII characters
    sanitized_question = sanitized_question.encode('ascii', 'ignore').decode()

    # Remove any URLs
    sanitized_question = re.sub(r'http\S+', '', sanitized_question)

    # Remove any mentions
    sanitized_question = re.sub(r'@\S+', '', sanitized_question)

    # Remove any hashtags
    sanitized_question = re.sub(r'#\S+', '', sanitized_question)

    # Remove any extra white spaces
    sanitized_question = re.sub(r'\s+', ' ', sanitized_question)

    # Limit the length of the question
    sanitized_question = sanitized_question[:1000]  # Adjust the maximum length as needed

    # remove code blocks
    sanitized_question = re.sub(r'```.*?```', '', sanitized_question, flags=re.DOTALL)

    # remove inline code
    sanitized_question = re.sub(r'`.*?`', '', sanitized_question)

    return sanitized_question


@app.route("/generate", methods=["POST"])
@limiter.limit("4/minute")
def generate():
    global mdai
    user_id = session.get('session')
    logger.debug(f"User {user_id} generate question")
    logger.log("ACCESS-Lv2", f"User {user_id} generate question")
    logger.log("SECURITY-Lv2", f"User {user_id} generate question")
    logger.info(f"User {user_id} generate question")
    user = db.get_user_by_id(user_id)
    if user :
        mdai.user_name = user.name
        mdai.regeneratePrompt()
    else:
        mdai = MDai()
        mdai.regeneratePrompt()
    question = question_sanitizer(request.form["question"])
    response = mdai.generate(question)
    return jsonify({"response": response})


@app.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    user_id = session.get('session')
    if user_id is None:
        logger.error(f"User {session.get('session')} not logged in")
        logger.debug(f"User {session.get('session')} not logged in")
        logger.log("DENNIED", f"User {session.get('session')} not logged in")
        flash("You need to login first", "danger")
        return redirect(url_for('login'))
    else:
        logged_in = True
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image_path = user.image
        logger.debug(f"User {name} access home")
        logger.info(f"User {name} access home")
        logger.log("ACCESS-Lv2", f"User {name} access home")
        redirections = db.get_route()
        try:
            projects_computer = db.get_two_latest_projects_by_type("computer_science")
            projects_robotics = db.get_two_latest_projects_by_type("robotics")
            projects_personal = db.get_two_latest_projects_by_type("personal")
            projects_work = db.get_two_latest_projects_by_type("work")
            projects = projects_computer + projects_robotics + projects_personal + projects_work
            for project in projects:
                project.parts = db.get_parts_by_project_id(project.id)

        except Exception as e:
            logger.error(f"Error getting projects: {e}")
            logger.log("SECURITY-Lv3", f"Error getting projects: {e}")
            projects = []
            flash("Error getting projects", "danger")

        return render_template('home.html', name=name, profile_image_path=profile_image_path, logged_in=logged_in,
                               role=user.role, routes=redirections, projects=projects)

@app.route('/projects', methods=['GET', 'POST'])
def projects():
    user_id = session.get('session')
    logger.debug(f"User {session.get('session')} access projects")
    logger.info(f"User {session.get('session')} access projects")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} access projects")
    redirections = db.get_route()
    allProjects = db.get_all_projects()
    for project in allProjects:
        project.parts = db.get_parts_by_project_id(project.id)
    if user_id is None:
        loged_in = False
        return render_template('projects.html', loged_in=loged_in, projects=allProjects, routes=redirections)
    else:
        logged_in = True
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image_path = user.image
        logger.debug(f"User {name} access home")
        logger.info(f"User {name} access home")
        logger.log("ACCESS-Lv2", f"User {name} access home")
        redirections = db.get_route()
        user = db.get_user_by_id(user_id)
        context = {'name': name, 'profile_image_path': profile_image_path, 'logged_in': logged_in, 'role': user.role,
                   'routes': redirections, 'projects': allProjects}
        return render_template('projects.html', **context)


@app.route('/project/<int:project_id>')
def project(project_id):
    user_id = session.get('session')
    logger.debug(f"User {session.get('session')} access project {project_id}")
    logger.info(f"User {session.get('session')} access project {project_id}")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} access project {project_id}")
    redirections = db.get_route()
    project_asked = db.get_project_by_id(project_id)
    project_asked.parts = db.get_parts_by_project_id(project_asked.id)
    if user_id is None:
        loged_in = False
        return render_template('project.html', loged_in=loged_in, project=project_asked, routes=redirections)
    else :
        logged_in = True
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image_path = user.image
        context = {'name': name, 'profile_image_path': profile_image_path, 'logged_in': logged_in, 'role': user.role,
                     'routes': redirections, 'project': project_asked}
        return render_template('project.html', **context)

@app.route('/resume')
def resume():
    user_id = session.get('session')
    logger.debug(f"User {session.get('session')} access resume")
    logger.info(f"User {session.get('session')} access resume")
    logger.log("ACCESS-Lv1", f"User {session.get('session')} access resume")
    about_me = db.get_about_me()
    about_me.contact_info = db.get_contact_info()
    zipped_testimonials = zip(about_me.testimonials_author, about_me.testimonials)
    redirections = db.get_route()
    if user_id is None:
        loged_in = False
        return render_template('resume.html', loged_in=loged_in,about_me=about_me,testimonials=zipped_testimonials,routes=redirections)
    else:
        loged_in = True
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image_path = user.image
        return render_template('resume.html', name=name, profile_image_path=profile_image_path, logged_in=loged_in,
                               role=user.role, about_me=about_me,testimonials=zipped_testimonials,routes=redirections)


@app.route('/robotics', methods=['GET'])
def robotics():
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        logged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        logged_in = False
        role = None
    logger.debug(f"User {name} access robotics")
    logger.info(f"User {name} access robotics")
    logger.log("ACCESS-Lv1", f"User {name} access robotics")

    try:
        projects = db.get_project_by_type("robotics")
    except Exception as e:
        logger.error(f"Error getting projects: {e}")
        logger.debug(f"Error getting projects: {e}")
        projects = None
    for project in projects:
        project.parts = db.get_parts_by_project_id(project.id)

    redirections = db.get_route()
    context = {
        'routes': redirections,
        'name': name,
        'profile_image_path': profile_image,
        'logged_in': logged_in,
        'role': role,
        'projects': projects
    }

    if projects:
        return render_template('robotics.html', **context)
    else:
        return render_template('workinprogress.html',routes=redirections)


@app.route('/personal', methods=['GET'])
def personal():
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        logged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        logged_in = False
        role = None
    logger.debug(f"User {name} access personal")
    logger.info(f"User {name} access personal")
    logger.log("ACCESS-Lv1", f"User {name} access personal")
    try:
        projects = db.get_project_by_type("personal")
    except Exception as e:
        logger.error(f"Error getting projects: {e}")
        logger.debug(f"Error getting projects: {e}")
        projects = None
    for project in projects:
        project.parts = db.get_parts_by_project_id(project.id)
    redirections = db.get_route()
    context = {
        'routes': redirections,
        'name': name,
        'profile_image_path': profile_image,
        'logged_in': logged_in,
        'role': role,
        'projects': projects
    }

    if projects:
        return render_template('personal.html', **context)
    else:
        return render_template('workinprogress.html',routes=redirections)


@app.route('/computer', methods=['GET'])
def computer():
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        logged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        logged_in = False
        role = None
    logger.debug(f"User {name} access computer")
    logger.info(f"User {name} access computer")
    logger.log("ACCESS-Lv1", f"User {name} access computer")
    try:
        projects = db.get_project_by_type("computer_science")
    except Exception as e:
        logger.error(f"Error getting projects: {e}")
        logger.debug(f"Error getting projects: {e}")
        projects = None
    for project in projects:
        project.parts = db.get_parts_by_project_id(project.id)
    redirections = db.get_route()
    context = {
        'routes': redirections,
        'name': name,
        'profile_image_path': profile_image,
        'logged_in': logged_in,
        'role': role,
        'projects': projects
    }

    if projects:
        return render_template('computer.html', **context)
    else:
        return render_template('workinprogress.html',routes=redirections)

@app.route('/work', methods=['GET'])
def work():
    user_id = session.get('session')
    if user_id is not None:
        user = db.get_user_by_id(user_id)
        name = user.name
        profile_image = user.image
        logged_in = True
        role = user.role
    else:
        name = None
        profile_image = None
        logged_in = False
        role = None
    logger.debug(f"User {name} access work")
    logger.info(f"User {name} access work")
    logger.log("ACCESS-Lv1", f"User {name} access work")
    try:
        projects = db.get_project_by_type("work")
    except Exception as e:
        logger.error(f"Error getting projects: {e}")
        logger.debug(f"Error getting projects: {e}")
        projects = None
    for project in projects:
        project.parts = db.get_parts_by_project_id(project.id)
    redirections = db.get_route()
    context = {
        'routes': redirections,
        'name': name,
        'profile_image_path': profile_image,
        'logged_in': logged_in,
        'role': role,
        'projects': projects
    }

    if projects:
        return render_template('workproject.html', **context)
    else:
        return render_template('workinprogress.html',routes=redirections)
@app.route('/addproject', methods=['GET', 'POST'])
@admin_required
def add_project():
    # Input validation and sanitization for user_id.
    user_id = session.get('session')
    if not user_id:
        flash("Invalid user ID", "danger")
        return redirect(url_for('home'))

    user = db.get_user_by_id(user_id)
    if user is None:
        flash("User not found", "danger")
        return redirect(url_for('home'))

    # Check if user is an admin.
    if user.role != "admin":
        logger.warning(f"User {user.name} tried to access add project page")
        logger.debug(f"User {user.name} tried to access add project page")
        logger.info(f"User {user.name} tried to access add project page")
        logger.log("DENIED", f"User {user.name} tried to access add project page")
        logger.log("SECURITY", f"User {user.name} tried to access add project page")
        flash("You need to be admin to add a project", "danger")
        return redirect(url_for('home'))

    logger.debug(f"User {user.name} access add project page")
    logger.info(f"User {user.name} access add project page")
    logger.log("ACCESS-Lv3", f"User {user.name} access add project page")
    if request.method == 'POST':
        form_data = request.form
        form_media = request.files
        # Input validation and sanitization for required fields.
        project_title = form_data.get('project_title')
        gitlab_link = form_data.get('gitlab_link')
        project_type = form_data.get('project_type')
        active_indicator = form_data.get('active_indicator')
        team_role = form_data.get('team_role')
        team_size = form_data.get('team-size')
        description = form_data.get('description')
        num_parts = form_data.get('num-parts')
        project_conclusion = form_data.get('project-conclusion')
        project_main_image = form_media.get('project_main_image')
        images_list = form_media.getlist('images_list')
        movies_list = form_media.getlist('movies_list')

        # main image
        # set a default image
        project_main_image_path = os.path.join(IMAGE_DIR, 'pics_ecran.webp')
        if project_main_image.filename != '':
            # check if image is alraedy in the folder
            if not os.path.isfile(os.path.join(IMAGE_DIR, project_main_image.filename)):
                project_main_image.save(os.path.join(IMAGE_DIR, project_main_image.filename))
                project_main_image_path = os.path.join(IMAGE_DIR, project_main_image.filename)
        # images
        images_path = []
        if images_list:
            for image in images_list:
                if image.filename != '':
                    image.path = os.path.join(IMAGE_DIR, image.filename)
                    # check if image is alraedy in the folder
                    if not os.path.isfile(image.path):
                        image.save(image.path)
                        logger.debug(f"Image {image.filename} saved")
                        logger.info(f"Image {image.filename} saved")
                        logger.log("SECURITY-Lv3", f"Image {image.filename} uploaded")
                    images_path.append(image.path)

        movies_path = []
        if movies_list:
            for movie in movies_list:
                if movie.filename != '':
                    movie.path = os.path.join(IMAGE_DIR, movie.filename)
                    # check if image is alraedy in the folder
                    if not os.path.isfile(movie.path):
                        movie.save(movie.path)
                        logger.debug(f"Movie {movie.filename} saved")
                        logger.info(f"Movie {movie.filename} saved")
                        logger.log("SECURITY-Lv3", f"Movie {movie.filename} uploaded")
                    movies_path.append(movie.path)

        new_project = Project(
            name=project_title,
            description=description,
            image=project_main_image_path,
            type=project_type,
            gitlab_link=gitlab_link,
            active=active_indicator,
            team_role=team_role,
            team_size=team_size,
            image_list=",".join(images_path),
            movies_list=",".join(movies_path),
            conclusion=project_conclusion
        )
        project_id = db.insert_project(new_project)

        for i in range(1, int(num_parts) + 1):
            try:
                title = form_data.get(f'part{i}-title')
                description = form_data.get(f'part{i}-description')
                codeLanguage = form_data.get(f'part{i}-addcode-lang')
                addcode = form_data.get(f'part{i}-addcode')
                conclusion = form_data.get(f'part{i}-conclusion')
                partIndex = i
                part_images_list = form_media.getlist(f'part{i}-images')

                part_images_paths = []
                if part_images_list:
                    for image in part_images_list:
                        if image.filename != '':
                            image.path = os.path.join(IMAGE_DIR, image.filename)
                            # check if image is alraedy in the folder
                            if not os.path.isfile(image.path):
                                image.save(image.path)
                                logger.debug(f"Image {image.filename} saved")
                                logger.info(f"Image {image.filename} saved")
                                logger.log("SECURITY-Lv3", f"Image {image.filename} uploaded")
                            part_images_paths.append(image.path)

                part = Part(
                    title=title,
                    description=description,
                    image=",".join(part_images_paths),
                    project_id=project_id,
                    codeLanguage=codeLanguage,
                    code=addcode,
                    conclusion=conclusion,
                    partIndex=partIndex
                )
                db.insert_part(part)
                logger.debug(f"Part {part.title} added to project {new_project.name}")
                logger.info(f"Part {part.title} added to project {new_project.name}")
                logger.log("SECURITY-Lv1", f"Part {part.title} added to project {new_project.name}")
            except Exception as e:
                logger.debug(f"Error while adding part to project {new_project.name}: {e}")
                logger.info(f"Error while adding part to project {new_project.name}")
                logger.error(f"Error while adding part to project {new_project.name}: {e}")
                flash("Error while adding part", "danger")
                return redirect(url_for('add_project'))
        flash("Project added successfully", "success")
        return redirect(url_for('home'))
    redirections = db.get_route()
    return render_template('addproject.html', name=user.name, profile_image_path=user.image, logged_in=True,
                           role=user.role, routes=redirections)


@app.route('/updateproject', methods=['GET', 'POST'])
@admin_required
def update_project():
    # Input validation and sanitization for user_id.
    user_id = session.get('session')
    if not user_id:
        logger.debug(f"Invalid user ID in update project")
        logger.info(f"Invalid user ID in update project")
        logger.log("SECURITY-Lv2", f"Invalid user ID in update project")
        flash("Invalid user ID", "danger")
        return redirect(url_for('home'))

    user = db.get_user_by_id(user_id)
    if user is None:
        logger.debug(f"User not found in update project")
        logger.info(f"User not found in update project")
        logger.log("SECURITY-Lv2", f"User not found in update project")
        flash("User not found", "danger")
        return redirect(url_for('home'))

    # Check if user is an admin.
    if user.role != "admin":
        logger.debug(f"User {user.name} tried to update a project")
        logger.info(f"User {user.name} tried to update a project")
        logger.log("SECURITY-Lv2", f"User {user.name} tried to update a project")
        flash("You need to be admin to update a project", "danger")
        return redirect(url_for('home'))

    # get projects
    projects = db.get_all_projects()
    for project in projects:
        project.parts = db.get_parts_by_project_id(project.id)

    if request.method == 'POST':
        print(request.form)
        form_data = request.form
        form_media = request.files
        # Input validation and sanitization for required fields.
        project_id = form_data.get('project_id')
        removed_part = form_data.get('removed-part')
        project_title = form_data.get('project_title')
        gitlab_link = form_data.get('gitlab_link')
        project_type = form_data.get('project_type')
        active_indicator = form_data.get('active_indicator')
        team_role = form_data.get('team_role')
        team_size = form_data.get('team-size')
        project_description = form_data.get('description')
        num_parts = form_data.get('num-parts')
        deleted_images = form_data.getlist('deleted-images-list')
        deleted_movies = form_data.getlist('deleted-videos-list')
        project_conclusion = form_data.get('project-conclusion')

        logger.debug(f"Updating project {project_title}")
        logger.info(f"Updating project {project_title}")
        logger.log("ACCESS-Lv2", f"{user.name} Updating project {project_title}")
        logger.log("SECURITY-Lv1", f"{user.name} Updating project {project_title}")

        if removed_part is not None and removed_part != '':
            for part_id in removed_part.split(',')[:-1]:
                if part_id:
                    try:
                        logger.debug(f"Removing part {part_id}")
                        logger.info(f"Removing part {part_id}")
                        db.remove_part_by_id(part_id)
                    except Exception as e:
                        print(e)
                        logger.debug(f"Failed to remove part: {part_id}")
                        logger.info(f"Failed to remove part: {part_id}")
                        logger.error(f"Failed to remove part: {part_id} : {e}")
                        flash(f"Failed to remove part: {part_id}", "danger")
                else:
                    # the part is new and not yet saved in the database
                    pass

        # save new main image for the project. If no new image uploaded, get the old one.
        project_main_image = form_media.get('project_main_image')
        if project_main_image.filename:
            project_main_image_path = os.path.join(IMAGE_DIR, project_main_image.filename)
            project_main_image.save(project_main_image_path)
            logger.debug(f"Saving new main image for project {project_title}")
            logger.info(f"Saving new main image for project {project_title}")
            logger.log("SECURITY-Lv2", f"Saving new main image for project {project_title}")
        else:
            project_main_image_path = db.get_project_main_image(project_id)

        # Save new images for the project. If no new images uploaded, get the old ones.
        images_paths = db.get_project_images(project_id)
        images_list = form_media.getlist('images-list')
        if images_list:
            for image in images_list:
                if image.filename:
                    image_path = os.path.join(IMAGE_DIR, image.filename)
                    image.save(image_path)
                    logger.debug(f"Saving new image for project {project_title}")
                    logger.info(f"Saving new image for project {project_title}")
                    logger.log("SECURITY-Lv2", f"Saving new image for project {project_title}")
                    images_paths.append(image_path)

        movies_paths = db.get_project_movies(project_id)
        movies_list = form_media.getlist('videos_list')
        # Save new movies for the project.
        if movies_list:
            for movie in movies_list:
                if movie.filename:
                    movie_path = os.path.join(MOVIE_DIR, movie.filename)
                    movie.save(movie_path)
                    logger.debug(f"Saving new movie for project {project_title}")
                    logger.info(f"Saving new movie for project {project_title}")
                    logger.log("SECURITY-Lv2", f"Saving new movie for project {project_title}")
                    movies_paths.append(movie_path)

        # delete images that were deleted by the user
        if deleted_images:
            for image in deleted_images[0].split(',')[:-1]:
                if db.check_image_used(image, project_id, None):
                    logger.debug(f"image {image}link removed from the project but still used by another project")
                    logger.info(f"image {image}link removed from the project but still used by another project")
                    logger.warning(f"image {image}link removed from the project but still used by another project")
                    # remove only the image from the images_paths list
                    images_paths.remove(image)
                else:
                    try:
                        logger.debug(f"image {image} was completely removed")
                        logger.info(f"image {image} was completely removed")
                        logger.log("SECURITY-Lv2", f"image {image} was completely removed")
                        images_paths.remove(image)
                        # remove the image from the images folder
                        os.remove(image)
                    except Exception as e:
                        logger.debug(f"image {image} was already removed")
                        logger.info(f"image {image} was already removed")
                        logger.error(f"image {image} was already removed : {e}")

        # delete movies that were deleted by the user
        if deleted_movies:
            for movie in deleted_movies[0].split(',')[:-1]:
                if db.check_movie_used(movie, project_id):
                    # remove only the movie from the movies_paths list
                    movies_paths.remove(movie)
                    logger.debug(f"image {movie}link removed from the project but still used by another project")
                    logger.info(f"image {movie}link removed from the project but still used by another project")
                    logger.warning(f"image {movie}link removed from the project but still used by another project")
                else:
                    try:
                        os.remove(movie)
                        movies_paths.remove(movie)
                        logger.debug(f"image {movie} was completely removed")
                    except:
                        logger.debug(f"image {movie} was already removed")
                        logger.info(f"image {movie} was already removed")
                        logger.error(f"image {movie} was already removed : {e}")

        # Input validation and sanitization for part fields.
        for i in range(1, int(num_parts) + 1):
            part_id = form_data.get(f'part{i}-id')
            title = form_data.get(f'part{i}-title')
            part_description = form_data.get(f'part{i}-description')
            codeLanguage = form_data.get(f'part{i}-addcode-lang')
            addcode = form_data.get(f'part{i}-addcode')
            conclusion = form_data.get(f'part{i}-conclusion')
            images = form_media.getlist(f'part{i}-images')
            deleted_images = form_data.getlist(f'deleted-images-list-part{i}')
            # get stored images for the part
            part_images_paths = db.get_part_images(part_id)
            if images:
                for image in images:
                    if image.filename:
                        image_path = os.path.join(IMAGE_DIR, image.filename)
                        image.save(image_path)
                        logger.debug(f"Saving new image for part {title}")
                        logger.info(f"Saving new image for part {title}")
                        logger.log("SECURITY-Lv2", f"{user.name} Saving new image for part {title}")
                        part_images_paths.append(image_path)

            # delete images that were deleted by the user
            if deleted_images:
                project_id = db.get_project_id(part_id)
                for image in deleted_images[0].split(',')[:-1]:
                    if db.check_image_used(image, project_id,part_id):
                        # remove only the image from the images_paths list
                        part_images_paths.remove(image)
                        logger.debug(f"image {image}link removed from the part but still used by another part")
                        logger.info(f"image {image}link removed from the part but still used by another part")
                        logger.warning(f"image {image}link removed from the part but still used by another part")
                    else:
                        try:
                            part_images_paths.remove(image)
                            # remove the image from the images folder
                            os.remove(image)
                            logger.debug(f"image {image} was completely removed")
                            logger.info(f"image {image} was completely removed")
                            logger.log("SECURITY-Lv2", f"image {image} was completely removed")
                        except Exception as e:
                            logger.debug(f"image {image} was already removed")
                            logger.info(f"image {image} was already removed")
                            logger.error(f"image {image} was already removed : {e}")

            try:
                logger.debug(f"Updating part {i}")
                logger.info(f"Updating part {i}")
                if part_id == '' or part_id is None:
                    part_id = -1
                else:
                    part_id = int(part_id)
                new_part = Part(title=title, description=part_description, codeLanguage=codeLanguage, code=addcode,
                                conclusion=conclusion, image=','.join(str(item) for item in part_images_paths),
                                project_id=project_id, id=part_id, partIndex=i)
                if new_part.id == -1:
                    # add new part to the database
                    db.insert_part(new_part)
                else:
                    # update part in the database
                    db.update_part(new_part)
            except Exception as e:
                print(e)
                logger.debug(f"Error updating part {i}")
                logger.info(f"Error updating part {i}")
                logger.error(f"Error updating part {i} : {e}")
                flash(f"Error updating part {i}", "danger")

        try:
            logger.debug(f"Updating project {project_title}")
            logger.info(f"Updating project {project_title}")
            logger.log("ACCESS-Lv3", f"{user.name} Updating project {project_title}")
            # update project in the database
            new_project = Project(name=project_title,
                                  description=project_description,
                                  image=project_main_image_path,
                                  type=project_type,
                                  gitlab_link=gitlab_link,
                                  active=active_indicator,
                                  team_role=team_role,
                                  team_size=team_size,
                                  image_list=','.join(str(item) for item in images_paths),
                                  movies_list=','.join(str(item) for item in movies_paths),
                                  conclusion=project_conclusion,
                                  id=project_id)
            db.update_project(new_project)
            logger.debug(f"Project updated successfully")
            logger.info(f"Project updated successfully")
            logger.log("ACCESS-Lv3", f"{user.name} Project updated successfully")
            flash("Project updated successfully", "success")
            # redirect to project page
            return redirect(url_for('home'))
        except Exception as e:
            logger.debug(f"Error updating {project_title}")
            logger.info(f"Error updating {project_title}")
            logger.error(f"Error updating {project_title} : {e}")
            flash(f"Error updating {project_title}", "danger")
    redirections = db.get_route()
    context = {
        'routes': redirections,
        'name': user.name,
        'profile_image_path': user.image,
        'logged_in': True,
        'role': user.role,
        'project_list': projects
    }
    return render_template('updateProject.html', **context)


# Delete project
@app.route('/deleteProject/<int:project_id>', methods=['POST'])
@lexit_required
def delete_project(project_id):
    if session.get('session') is not None:
        user = db.get_user_by_id(session.get('session'))
        name = user.name
        profile_image = user.image
        loged_in = True
        role = user.role
        logger.debug(f"User {name}({user.id}) is trying to delete Project id:{project_id}")
        logger.info(f"User {name}({user.id}) is trying to delete Project id:{project_id}")
        logger.log("ACCESS-Lv3", f"{user.name} is trying to delete Project id:{project_id}")
        logger.log("SECURITY-Lv2", f"{user.name} is trying to delete Project id:{project_id}")
        # check if admin is "Lexit"
        if role != "admin":
            logger.log("SECURITY-Lv3", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            logger.log("DENNIED", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            flash("You are not authorized to delete projects", "danger")
            return redirect(url_for('home'))
        elif name != "LexIt":
            logger.log("SECURITY-Lv3", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            logger.log("DENNIED", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            flash("Only LexIt is able to delete projects", "danger")
            return redirect(url_for('update_project'))

        if role == "admin" and name == "LexIt":
            try:
                logger.debug(f"Deleting project {project_id}")
                logger.info(f"Deleting project {project_id}")
                logger.log("ACCESS-Lv3", f"{user.name} Deleting project {project_id}")
                project_name = db.get_project_name(project_id)
            except Exception as e:
                flash(f"Error project {project_id} not found", "danger")
                logger.debug(f"Error project {project_id} not found")
                logger.info(f"Error project {project_id} not found")
                logger.error(f"Error project {project_id} not found : {e}")
                return redirect(url_for('update_project'))
            try:
                logger.debug(f"Deleting project {project_id} from database")
                logger.info(f"Deleting project {project_id} from database")
                logger.log("ACCESS-Lv3", f"{user.name} Deleting project {project_id} from database")
                logger.log("SECURITY-Lv2", f"{user.name} Deleting project {project_id} from database")
                db.remove_part_by_project_id(project_id)
                db.remove_project_by_id(project_id)
                flash(f"Project {project_name} deleted successfully", "success")
            except Exception as e:
                flash(f"Error deleting {project_name}", "danger")
                logger.debug(f"Error deleting {project_name}")
                logger.info(f"Error deleting {project_name}")
                logger.error(f"Error deleting {project_name} : {e}")
            return redirect(url_for('update_project'))
        else:
            logger.log("SECURITY-Lv3", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            logger.log("DENNIED", f"{user.name}({user.id}) tried to delete Project id:{project_id}")
            flash("You are not authorized to delete projects", "danger")
            return redirect(url_for('update_project'))
    else:
        logger.log("SECURITY-Lv3", f"Anonymous tried to delete Project id:{project_id}")
        logger.log("DENNIED", f"Anonymous tried to delete Project id:{project_id}")
        flash("You are not authorized to delete projects", "danger")
        return redirect(url_for('update_project'))


if __name__ == '__main__':
    try:
        app.run()
        logger.info("Server started successfully")
    except Exception as e:
        logger.critical(f"Error starting server : {e}")
        logger.info("Server started successfully")
