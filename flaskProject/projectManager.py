import datetime
import json

class Project:
    parts = []

    def __init__(self, name, description, image, type, gitlab_link, active, team_role, team_size, image_list,
                 movies_list, conclusion, id=None, created_at=datetime.datetime.now()):
        self.id = id
        self.name = name
        self.description = description
        self.image = image
        self.type = type
        self.gitlab_link = gitlab_link
        self.active = active
        self.created_at = created_at
        self.team_role = team_role
        self.team_size = team_size
        self.image_list = image_list
        self.movies_list = movies_list
        self.conclusion = conclusion

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "image": self.image,
            "type": self.type,
            "gitlab_link": self.gitlab_link,
            "active": self.active,
            "created_at": self.created_at,
            "team_role": self.team_role,
            "team_size": self.team_size,
            "image_list": self.image_list,
            "movies_list": self.movies_list,
            "conclusion": self.conclusion,
            "parts": self.parts
        }

    def __str__(self):
        return f"Project(id={self.id}, name={self.name}, description={self.description}, image={self.image}, type={self.type}, gitlab_link={self.gitlab_link}, active={self.active}, created_at={self.created_at}, team_role={self.team_role}, team_size={self.team_size}, image_list={self.image_list}, movies_list={self.movies_list}, conclusion={self.conclusion}, parts={self.parts})"


class Part:
    def __init__(self, title, description, image, project_id,codeLanguage, code, conclusion, id=None, partIndex=0):
        self.id = id
        self.title = title
        self.description = description
        self.image = image
        self.project_id = project_id
        self.codeLanguage = codeLanguage
        self.code = code
        self.conclusion = conclusion
        self.partIndex = partIndex

    def json(self):
        return {
            "id": self.id,
            "title": self.title,
            "description": self.description,
            "image": self.image,
            "project_id": self.project_id,
            "codeLanguage": self.codeLanguage,
            "code": self.code,
            "conclusion": self.conclusion,
            "partIndex": self.partIndex
        }

    def __str__(self):
        return f"Part(id={self.id}, title={self.title}, description={self.description}, image={self.image}, project_id={self.project_id},codeLanguage={self.codeLanguage} ,code={self.code}, conclusion={self.conclusion}, partIndex={self.partIndex})"
