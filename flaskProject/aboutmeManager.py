from datetime import date
class AboutMe:
    def __init__(self, firstname="", lastname="",pseudo="", date_of_birth=date(1998, 11, 10), introduction="", background="", experience="",
                 hard_skills=[],hard_skills_os=[],hard_skills_os_highlights=[],hard_skills_infra=[] ,hard_skills_infra_highlights=[],hard_skills_network=[],hard_skills_network_highlights=[],
                    hard_skills_web=[],hard_skills_web_highlights=[],hard_skills_mobile=[],hard_skills_mobile_highlights=[],hard_skills_ai=[],hard_skills_ai_highlights=[],
                    hard_skills_dev=[],hard_skills_dev_highlights=[],hard_skills_data=[],hard_skills_data_highlights=[],hard_skills_cloud=[],hard_skills_cloud_highlights=[],
                    hard_skills_security=[],hard_skills_security_highlights=[],hard_skills_electronic=[],hard_skills_electronic_highlights=[],hard_skills_mechatronics=[],hard_skills_mechatronics_highlights=[],
                    hard_skills_embedded_dev=[],hard_skills_embedded_dev_highlights=[],hard_skills_other=[],hard_skills_other_highlights=[],
                 soft_skills=[],soft_skills_work=[],languages=[], achievements=[], testimonials=[],testimonials_author=[], contact_info=None, contact_info_id=None,
                 target_job="", resume="", hobbies=[]):
        self.firstname = firstname
        self.lastname = lastname
        self.pseudo = pseudo
        self.date_of_birth = date_of_birth
        self.introduction = introduction
        self.background = background
        self.experience = experience
        self.hard_skills = hard_skills
        self.hard_skills_os = hard_skills_os
        self.hard_skills_os_highlights = hard_skills_os_highlights
        self.hard_skills_infra = hard_skills_infra
        self.hard_skills_infra_highlights = hard_skills_infra_highlights
        self.hard_skills_network = hard_skills_network
        self.hard_skills_network_highlights = hard_skills_network_highlights
        self.hard_skills_web = hard_skills_web
        self.hard_skills_web_highlights = hard_skills_web_highlights
        self.hard_skills_mobile = hard_skills_mobile
        self.hard_skills_mobile_highlights = hard_skills_mobile_highlights
        self.hard_skills_ai = hard_skills_ai
        self.hard_skills_ai_highlights = hard_skills_ai_highlights
        self.hard_skills_dev = hard_skills_dev
        self.hard_skills_dev_highlights = hard_skills_dev_highlights
        self.hard_skills_data = hard_skills_data
        self.hard_skills_data_highlights = hard_skills_data_highlights
        self.hard_skills_cloud = hard_skills_cloud
        self.hard_skills_cloud_highlights = hard_skills_cloud_highlights
        self.hard_skills_security = hard_skills_security
        self.hard_skills_security_highlights = hard_skills_security_highlights
        self.hard_skills_electronic = hard_skills_electronic
        self.hard_skills_electronic_highlights = hard_skills_electronic_highlights
        self.hard_skills_mechatronics = hard_skills_mechatronics
        self.hard_skills_mechatronics_highlights = hard_skills_mechatronics_highlights
        self.hard_skills_embedded_dev = hard_skills_embedded_dev
        self.hard_skills_embedded_dev_highlights = hard_skills_embedded_dev_highlights
        self.hard_skills_other = hard_skills_other
        self.hard_skills_other_highlights = hard_skills_other_highlights
        self.soft_skills = soft_skills
        self.soft_skills_work = soft_skills_work
        self.languages = languages
        self.achievements = achievements
        self.testimonials = testimonials
        self.testimonials_author = testimonials_author
        self.contact_info = contact_info
        self.contact_info_id = None
        self.target_job = target_job
        self.resume = resume
        self.hobbies = hobbies





    def json(self):
        return {
            "firstname": self.firstname,
            "lastname": self.lastname,
            "pseudo": self.pseudo,
            "date_of_birth": self.date_of_birth,
            "introduction": self.introduction,
            "background": self.background,
            "experience": self.experience,
            "hard_skills": self.hard_skills,
            "hard_skills_os": self.hard_skills_os,
            "hard_skills_os_highlights": self.hard_skills_os_highlights,
            "hard_skills_infra": self.hard_skills_infra,
            "hard_skills_infra_highlights": self.hard_skills_infra_highlights,
            "hard_skills_network": self.hard_skills_network,
            "hard_skills_network_highlights": self.hard_skills_network_highlights,
            "hard_skills_web": self.hard_skills_web,
            "hard_skills_web_highlights": self.hard_skills_web_highlights,
            "hard_skills_mobile": self.hard_skills_mobile,
            "hard_skills_mobile_highlights": self.hard_skills_mobile_highlights,
            "hard_skills_ai": self.hard_skills_ai,
            "hard_skills_ai_highlights": self.hard_skills_ai_highlights,
            "hard_skills_dev": self.hard_skills_dev,
            "hard_skills_dev_highlights": self.hard_skills_dev_highlights,
            "hard_skills_data": self.hard_skills_data,
            "hard_skills_data_highlights": self.hard_skills_data_highlights,
            "hard_skills_cloud": self.hard_skills_cloud,
            "hard_skills_cloud_highlights": self.hard_skills_cloud_highlights,
            "hard_skills_security": self.hard_skills_security,
            "hard_skills_security_highlights": self.hard_skills_security_highlights,
            "hard_skills_electronic": self.hard_skills_electronic,
            "hard_skills_electronic_highlights": self.hard_skills_electronic_highlights,
            "hard_skills_mechatronics": self.hard_skills_mechatronics,
            "hard_skills_mechatronics_highlights": self.hard_skills_mechatronics_highlights,
            "hard_skills_embedded_dev": self.hard_skills_embedded_dev,
            "hard_skills_embedded_dev_highlights": self.hard_skills_embedded_dev_highlights,
            "hard_skills_other": self.hard_skills_other,
            "hard_skills_other_highlights": self.hard_skills_other_highlights,
            "soft_skills": self.soft_skills,
            "soft_skills_work": self.soft_skills_work,
            "languages": self.languages,
            "achievements": self.achievements,
            "testimonials": self.testimonials,
            "testimonials_author": self.testimonials_author,
            "contact_info": self.contact_info.json(),
            "contact_info_id": self.contact_info_id,
            "target_job": self.target_job,
            "resume": self.resume,
            "hobbies": self.hobbies
        }

    def __str__(self):
        return f"AboutMe(firstname={self.firstname}, lastname={self.lastname}, pseudo={self.pseudo}, " \
               f"date_of_birth={self.date_of_birth}, introduction={self.introduction}, background={self.background}," \
               f" experience={self.experience}, hard_skills={self.hard_skills}, soft_skills={self.soft_skills}," \
               f"languages={self.languages}, achievements={self.achievements}, testimonials={self.testimonials}, " \
               f"testimonials_author={self.testimonials_author}, contact_info={self.contact_info}, " \
               f" target_job={self.target_job}, resume={self.resume}, hobbies={self.hobbies})"
class ContactInfo:
    def __init__(self, personal_email,lexit_email, school_email, phone, address):
        self.personal_email = personal_email
        self.lexit_email = lexit_email
        self.school_email = school_email
        self.phone = phone
        self.address = address

    def json(self):
        return {
            "personal_email": self.personal_email,
            "lexit_email": self.lexit_email,
            "school_email": self.school_email,
            "phone": self.phone,
            "address": self.address
        }

    def __str__(self):
        return f"ContactInfo(personal_email={self.personal_email}, lexit_email={self.lexit_email}, school_email={self.school_email}, phone={self.phone}, address={self.address})"
