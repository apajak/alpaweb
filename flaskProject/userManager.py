class User:
    def __init__(self, id, name, password, email, role,image,token):
        self.id = id
        self.name = name
        self.password = password
        self.email = email
        self.role = role
        self.image = image
        self.token = token

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "role": self.role,
            "image": self.image,
            "token": self.token
        }
    def __str__(self):
        return f"User(id={self.id}, name={self.name}, password={self.password}, email={self.email}, role={self.role},image={self.image},token={self.token})"


