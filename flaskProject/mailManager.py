import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.utils import make_msgid
import base64

class PasswordResetEmailSender:
    def __init__(self, smtp_server, smtp_port, sender_email, sender_password):
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.sender_email = sender_email
        self.sender_password = sender_password

    def send_password_reset_email(self, receiver_email, token):
        print(f"Sending password reset email to {receiver_email}")
        reset_link = f"http://lexit.cloud/resetPassword?token={token}"
        print(f"Reset link: {reset_link}")
        receiver_name = receiver_email.split("@")[0].replace(".", " ")
        # Capitalize the first letter of each word
        receiver_name = receiver_name.title()
        sender_name = "A.C.E"
        subject = "Password Reset"
        # Create the HTML body with logo, message, and signature
        logo_path = "./static/icons/alpagam_logo_wbg.jpg"
        logo_cid = make_msgid()
        with open(logo_path, "rb") as f:
            logo_data = f.read()
        logo_base64 = base64.b64encode(logo_data).decode("ascii")

        message = f"""
            <html>
                <head>
                    <style>
                        .brand-container {{
                            width: 100%;
                            height: 100px;
                            background-color: #4b6070;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }}
                        .brand-background {{
                            width: 100%;
                            height: 100%;
                            background-color: #f8f9fa;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }}
                        .brand-logo {{
                            width: 100px;
                            height: 100px;
                            object-fit: contain;
                            border-radius: 50%;
                        }}
                        .brand {{
                            font-size: 40px;
                            color: #6c757d;
                            margin-left: 10px;
                        }}
                    </style>
                </head>
                <body>
                    <div class="brand-container">
                        <div class="brand-background">
                            <img src="data:image/jpeg;base64,{logo_base64}" alt="logo" class="brand-logo">
                            <h1 class="brand">Lexit Cloud</h1>
                        </div>
                    </div>
                    <h2>Password Reset</h3>
                    <p>Hello {receiver_name},</p>
                    <p>You have requested to reset your password. Please click the link below to reset it:</p>
                    <p><a href="{reset_link}">{reset_link}</a></p>
                    <p>If you didn't request this, please ignore this email.</p>
                    <br>
                    <p>Best regards,</p>
                    <p>Your Website Team</p>
                    <p>{sender_name}</p>
                    <hr>
                    <p style="font-size: 12px; color: #6c757d;">This is an automated email. Please do not reply.</p>
                </body>
            </html>
        """


        # Create a multipart message
        msg = MIMEMultipart("alternative")
        msg["From"] = f"{sender_name} <{self.sender_email}>"
        msg["To"] = receiver_email
        msg["Subject"] = subject

        # Attach the HTML body
        msg.attach(MIMEText(message, "html"))

        # Attach the logo image
        logo_image = MIMEImage(logo_data)
        logo_image.add_header("Content-ID", logo_cid)
        msg.attach(logo_image)

        # Start the SMTP server connection
        with smtplib.SMTP(self.smtp_server, self.smtp_port) as server:
            server.starttls()
            server.login(self.sender_email, self.sender_password)
            server.send_message(msg)
            print("Email sent successfully")
