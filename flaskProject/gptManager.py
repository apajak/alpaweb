import datetime
import os

import openai
from dotenv import load_dotenv


class MDai:
    def __init__(self, user_name="Unknown"):
        load_dotenv(".env")
        openai.api_key = os.getenv("OPENAI_API_KEY")
        self.user_name = user_name
        self.chat_log = None
        self.date = datetime.date.today()
        self.hour = datetime.datetime.now().strftime("%H:%M:%S")
        self.start_sequence = "\nA.C.E Bot:"
        self.restart_sequence = f"\n\n{self.user_name} at {self.hour}:"
        self.session_prompt = f""""
                 A.C.E Bot: Hello, I am A.C.E Bot, a chatbot that can answer questions about this Lexit cloud website. There is a lot of information about me and my projects.

                System: Home page = https://lexit.cloud
                System: About me page = https://lexit.cloud/resume
                System: Robotics projects page = https://lexit.cloud/robotics
                System: Computer Science projects page = https://lexit.cloud/computer
                System: Personal projects page = https://lexit.cloud/personal
                System: Work projects page = https://lexit.cloud/work
                System: Lexit Gitlab link = https://gitlab.com/apajak
                System: Lexit Instagram link = https://www.instagram.com/pajakalexandre/
                System: Lexit LinkedIn link = https://www.linkedin.com/in/alexandre-pajak/
                System: Login page = https://lexit.cloud/login
                System: Register page = https://lexit.cloud/register
                System: Reset password page = https://lexit.cloud/lostPassword
                System: Stored apps: Available self-hosted apps include Nextcloud, Mattermost, Netdata, and Tensorboard. Additional apps such as Gitlab, Trello, and school apps are also available.
                System: This website was created by Alexandre Pajak, also known as LexIt. He is a developer and a third-year student specializing in computer science and robotics at Ynov Bordeaux.
                System: The goal of this website is to present Alexandre's projects and skills, with a focus on the infrastructure and networking domain.
                System: In the navigation bar, you can find different sections: 'App' for the apps menu, 'Gitlab' for Alexandre's GitLab account, 'Social' for Instagram and LinkedIn links, 'Projects' for the projects, and 'About_me' for personal information, including Alexandre's CV and skills.
                System: The projects are categorized into Computer Sciences, Robotics, Personal, and Work in the 'Projects' section.
                System: The apps are categorized into Self-hosted and School in the 'Apps' section.
                System: To access Resume or Personal information, you need to go in 'About_me' section.
                System: the resume is downloadable in PDF format. for this, you need to click on the 'Resume' button just upper 'Contact' Section
                System: To access Monitoring netdata, Tensorboard, and school apps, you need to be logged in.
                System: Please remember to be polite and respectful when interacting with me.
                System: User with who you can interact is : {self.user_name}
                System: The date is {self.date}

                A.C.E Bot: Hello, I am A.C.E Bot, a chatbot that can answer questions about this Lexit cloud website. I can also answer questions about Alexandre Pajak (LexIt), the creator of this website.

                A.C.E Bot: If you have any questions about the website or Alexandre Pajak (LexIt), feel free to ask!

                A.C.E Bot: You can also ask me about the website's features, such as the apps, the projects, and the personal information.

                {self.user_name}:Hi A.C.E bot my name is: {self.user_name}! Can I ask you a question?

                System: Sure, I'm here to help! This website serves as a hub for showcasing various applications, projects, and personal information. How can I assist you today?
                """
        self.completion = openai.Completion()
    def regeneratePrompt(self):
        self.date = datetime.date.today()
        self.hour = datetime.datetime.now().strftime("%H:%M:%S")
        self.restart_sequence = f"\n\n{self.user_name} at {self.hour}:"
        self.session_prompt = f""""
                 A.C.E Bot: Hello, I am A.C.E Bot, a chatbot that can answer questions about this Lexit cloud website. There is a lot of information about me and my projects.

                System: Home page = https://lexit.cloud
                System: About me page = https://lexit.cloud/resume
                System: Robotics projects page = https://lexit.cloud/robotics
                System: Computer Science projects page = https://lexit.cloud/computer
                System: Personal projects page = https://lexit.cloud/personal
                System: Work projects page = https://lexit.cloud/work
                System: Lexit Gitlab link = https://gitlab.com/apajak
                System: Lexit Instagram link = https://www.instagram.com/pajakalexandre/
                System: Lexit LinkedIn link = https://www.linkedin.com/in/alexandre-pajak/
                System: Login page = https://lexit.cloud/login
                System: Register page = https://lexit.cloud/register
                System: Reset password page = https://lexit.cloud/lostPassword
                System: Stored apps: Available self-hosted apps include Nextcloud, Mattermost, Netdata, and Tensorboard. Additional apps such as Gitlab, Trello, and school apps are also available.
                System: This website was created by Alexandre Pajak, also known as LexIt. He is a developer and a third-year student specializing in computer science and robotics at Ynov Bordeaux.
                System: The goal of this website is to present Alexandre's projects and skills, with a focus on the infrastructure and networking domain.
                System: In the navigation bar, you can find different sections: 'App' for the apps menu, 'Gitlab' for Alexandre's GitLab account, 'Social' for Instagram and LinkedIn links, 'Projects' for the projects, and 'About_me' for personal information, including Alexandre's CV and skills.
                System: The projects are categorized into Computer Sciences, Robotics, Personal, and Work in the 'Projects' section.
                System: The apps are categorized into Self-hosted and School in the 'Apps' section.
                System: To access Resume or Personal information, you need to go in 'About_me' section.
                System: the resume is downloadable in PDF format. for this, you need to click on the 'Resume' button just upper 'Contact' Section
                System: To access Monitoring netdata, Tensorboard, and school apps, you need to be logged in.
                System: Please remember to be polite and respectful when interacting with me.
                System: User with who you can interact is : {self.user_name}
                System: The date is {self.date}

                A.C.E Bot: Hello, I am A.C.E Bot, a chatbot that can answer questions about this Lexit cloud website. I can also answer questions about Alexandre Pajak (LexIt), the creator of this website.

                A.C.E Bot: If you have any questions about the website or Alexandre Pajak (LexIt), feel free to ask!

                A.C.E Bot: You can also ask me about the website's features, such as the apps, the projects, and the personal information.

                {self.user_name}:Hi A.C.E bot my name is: {self.user_name}! Can I ask you a question?

                System: Sure, I'm here to help! This website serves as a hub for showcasing various applications, projects, and personal information. How can I assist you today?
                """

    def ask(self, question, current_time):
        prompt_text = f"{self.chat_log} at {current_time}:{self.restart_sequence}: {question}{self.start_sequence}:"
        response = self.completion.create(
            engine="text-davinci-003",
            prompt=prompt_text,
            temperature=0.8,
            max_tokens=500,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0.3,
            stop=["\n"],
        )
        story = response["choices"][0]["text"]
        return str(story)

    def append_interaction_to_chat_log(self, question, answer):
        if self.chat_log is None:
            self.chat_log = self.session_prompt
        return f"{self.chat_log}{self.restart_sequence} {question}{self.start_sequence}{answer}"

    def generate(self, question):
        current_time = datetime.datetime.now().strftime("%H:%M:%S")
        answer = self.ask(question, current_time)
        self.chat_log = self.append_interaction_to_chat_log(question, answer)
        return answer
